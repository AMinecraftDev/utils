package net.aminecraftdev.utils.builders;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 05-Feb-18
 */
public interface IBuilder<T> {

    T build();

}
