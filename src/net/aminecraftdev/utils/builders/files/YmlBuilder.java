package net.aminecraftdev.utils.builders.files;

import net.aminecraftdev.utils.file.FileUtils;
import net.aminecraftdev.utils.builders.IBuilder;
import net.aminecraftdev.utils.module.ModuleManager;
import net.aminecraftdev.utils.file.YmlFile;

import java.io.File;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 05-Feb-18
 */
public class YmlBuilder implements IBuilder<YmlFile> {

    private boolean saveResource = false, moveOld = false;
    private File oldFile, newFile;

    public YmlBuilder(File newFile) {
        setFile(newFile);
    }

    public YmlBuilder setFile(File file) {
        this.newFile = file;
        return this;
    }

    public YmlBuilder setOldFile(File file) {
        this.oldFile = file;
        return this;
    }

    public YmlBuilder saveResource(boolean bool) {
        this.saveResource = bool;
        return this;
    }

    public YmlBuilder setMoveOld(boolean bool) {
        this.moveOld = bool;
        return this;
    }

    @Override
    public YmlFile build() {
        if(this.moveOld && this.oldFile != null && this.oldFile.exists()) {
            FileUtils.moveFile(this.oldFile, this.newFile);
        }

        if(!this.newFile.exists()) {
            if(this.saveResource) {
                ModuleManager.getPlugin().saveResource(this.newFile.getName(), false);
            } else {
                try {
                    this.newFile.createNewFile();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return new YmlFile(this.newFile);
    }
}
