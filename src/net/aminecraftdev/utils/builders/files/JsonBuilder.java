package net.aminecraftdev.utils.builders.files;

import com.google.gson.Gson;
import net.aminecraftdev.utils.file.FileUtils;
import net.aminecraftdev.utils.builders.IBuilder;
import net.aminecraftdev.utils.module.ModuleManager;
import net.aminecraftdev.utils.file.JsonFile;
import net.aminecraftdev.utils.serialization.BoostedGson;

import java.io.File;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 05-Feb-18
 */
public class JsonBuilder<T> implements IBuilder<JsonFile> {

    private boolean saveResource = false, moveOld = false, isArray = false;
    private File oldFile, newFile;
    private Class<T> clazz;
    private Gson gson;

    public JsonBuilder(File file) {
        setGson(BoostedGson.getGson());
        setFile(file);
    }

    public JsonBuilder<T> setFile(File file) {
        this.newFile = file;
        return this;
    }

    public JsonBuilder<T> setOldFile(File file) {
        this.oldFile = file;
        return this;
    }

    public JsonBuilder<T> saveResource(boolean bool) {
        this.saveResource = bool;
        return this;
    }

    public JsonBuilder<T> setMoveOld(boolean bool) {
        this.moveOld = bool;
        return this;
    }

    public JsonBuilder<T> setIsArray(boolean bool) {
        this.isArray = bool;
        return this;
    }

    public JsonBuilder<T> setGson(Gson gson) {
        this.gson = gson;
        return this;
    }

    public JsonBuilder<T> setClass(Class<T> clazz) {
        this.clazz = clazz;
        return this;
    }

    @Override
    public JsonFile<T> build() {
        if(this.moveOld && this.oldFile != null && this.oldFile.exists()) {
            FileUtils.moveFile(this.oldFile, this.newFile);
        }

        if(!this.newFile.exists()) {
            if(this.saveResource) {
                ModuleManager.getPlugin().saveResource(this.newFile.getName(), false);
            } else {
                try {
                    this.newFile.createNewFile();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return new JsonFile<>(this.newFile, this.isArray, this.gson, this.clazz);
    }
}
