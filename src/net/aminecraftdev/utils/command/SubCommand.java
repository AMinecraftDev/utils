package net.aminecraftdev.utils.command;

import org.bukkit.command.CommandSender;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 19-Sep-17
 */
public abstract class SubCommand {

    private String subCommand;

    public SubCommand(String subCommand) {
        this.subCommand = subCommand;
    }

    public String getSubCommand() {
        return this.subCommand;
    }

    public abstract void execute(CommandSender sender, String[] args);

}
