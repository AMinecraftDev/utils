package net.aminecraftdev.utils.command;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 19-Sep-17
 */
public interface SubCommandHandler {

    void addSubCommand(SubCommand subCommand);

}
