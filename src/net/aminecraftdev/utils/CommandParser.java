package net.aminecraftdev.utils;

import com.google.common.base.Charsets;
import net.aminecraftdev.utils.qplugin.QPlugin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 13-Dec-17
 */
public class CommandParser {

    private static CommandParser instance;
    private QPlugin plugin;

    public CommandParser(QPlugin plugin) {
        instance = this;
        this.plugin = plugin;

        Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(plugin, "QNUtils");
    }

    public void sendBungeeCommand(String command) {
        if(Bukkit.getServer().getOnlinePlayers().size() == 0) {
            ServerUtils.logError("You need at least one player online to send a Bungee command");
            return;
        }

        Player player = Bukkit.getServer().getOnlinePlayers().iterator().next();

        player.sendPluginMessage(this.plugin, "QNUtils", command.trim().getBytes(Charsets.UTF_8));
    }

    public static CommandParser get() {
        return instance;
    }

}
