package net.aminecraftdev.utils;

import net.aminecraftdev.utils.core.PlayerData;
import net.aminecraftdev.utils.dependencies.*;
import net.aminecraftdev.utils.google.youtube.YouTube;
import net.aminecraftdev.utils.inventory.Panel;
import net.aminecraftdev.utils.module.ModuleManager;
import net.aminecraftdev.utils.qplugin.QPlugin;
import net.aminecraftdev.utils.qplugin.SimpleQPlugin;
import net.aminecraftdev.utils.reflection.NMSVersionHandler;
import net.aminecraftdev.utils.reflection.ReflectionUtils;
import net.aminecraftdev.utils.voucher.Voucher;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.logging.Level;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 14-Dec-17
 */
public class UtilLoader {

    private static UtilLoader instance;

    private boolean hasBeenLoaded1 = false, hasBeenLoaded2;

    public UtilLoader() {
        instance = this;
    }

    public void loadFull(QPlugin plugin) {
        this.hasBeenLoaded2 = true;

        new ModuleManager(plugin);
        Panel.setPlugin(plugin);
        Voucher.setJavaPlugin(plugin);
        SubPlugin.setPlugin(plugin);

        if(attemptLoading(FactionHelper.class)) FactionHelper.get().isConnected();
        if(attemptLoading(TownyHelper.class)) TownyHelper.get().isConnected();
        if(attemptLoading(ASkyblockHelper.class)) ASkyblockHelper.get().isConnected();
        if(attemptLoading(EssentialsHelper.class)) EssentialsHelper.get().isConnected();
        if(attemptLoading(WorldGuardHelper.class)) WorldGuardHelper.get().isConnected();
        if(attemptLoading(PremiumVanishHelper.class)) PremiumVanishHelper.get().isConnected();

        new ReflectionUtils();
        new ServerUtils(plugin);

        ModuleManager.get().add(new YouTube());
        ModuleManager.get().reload();

        for(Player player : Bukkit.getOnlinePlayers()) {
            PlayerData.createPlayerData(player.getUniqueId());
        }
    }

    public boolean loadHalf(SimpleQPlugin plugin) {
        this.hasBeenLoaded1 = true;

        new NMSVersionHandler();

        if(!new VaultHelper().isConnected()) {
            Bukkit.getLogger().log(Level.WARNING, "Something went wrong when initializing the vault connection.");
            Bukkit.getLogger().log(Level.WARNING, "Disabling the plugin..");
            Bukkit.getPluginManager().disablePlugin(plugin);
            return false;
        }

        plugin.addListener(new PlayerData(plugin));

        for(Player player : Bukkit.getOnlinePlayers()) {
            PlayerData.createPlayerData(player.getUniqueId());
        }

        return true;
    }

    public boolean hasBeenLoaded1() {
        return this.hasBeenLoaded1;
    }

    public boolean hasBeenLoaded2() {
        return this.hasBeenLoaded2;
    }

    private boolean attemptLoading(Class<?> clazz) {
        try {
            clazz.newInstance();
            return true;
        } catch (NoClassDefFoundError | IllegalAccessException | InstantiationException ex) {
            return false;
        }
    }

    public static UtilLoader get() {
        return instance;
    }

}
