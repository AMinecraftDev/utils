package net.aminecraftdev.utils.core;

import net.aminecraftdev.utils.Reloadable;
import net.aminecraftdev.utils.builders.files.YmlBuilder;
import net.aminecraftdev.utils.file.YmlFile;
import net.aminecraftdev.utils.qplugin.SimpleQPlugin;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.io.File;
import java.util.*;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 24-May-17
 */
public class PlayerData implements Listener, Reloadable {

    private static Map<String, Object> defaultObjects = new HashMap<>();
    private static SimpleQPlugin PLUGIN;
    private static YmlFile FILE;

    public PlayerData(SimpleQPlugin simpleQPlugin) {
        PLUGIN = simpleQPlugin;

        reload();
    }

    @Override
    public void reload() {
        File file1 = new File(PLUGIN.getPluginData(), "player-data.yml");
        File file2 = new File(PLUGIN.getDataFolder(), "player-data.yml");

        FILE = new YmlBuilder(file1)
                .setOldFile(file2)
                .setMoveOld(true)
                .saveResource(false)
                .build();

        for(String uuid : FILE.getConfig().getKeys(false)) {
            for(String path : defaultObjects.keySet()) {
                FILE.getConfig().addDefault(uuid + "." + path, defaultObjects.get(path));
            }
        }

        FILE.getConfig().options().copyDefaults(true);
        FILE.saveFile(FILE.getConfig());
    }

    public static Queue<ConfigurationSection> getPlayerConfigSections() {
        Queue<ConfigurationSection> configurationSections = new LinkedList<>();
        ConfigurationSection configurationSection = FILE.getConfig().getConfigurationSection("Players");

        for(String uuid : configurationSection.getKeys(false)) {
            configurationSections.add(configurationSection.getConfigurationSection(uuid));
        }

        return configurationSections;
    }

    public static Queue<ConfigurationSection> getIPConfigSections() {
        Queue<ConfigurationSection> configurationSections = new LinkedList<>();
        ConfigurationSection configurationSection = FILE.getConfig().getConfigurationSection("IPs");

        for(String ip : configurationSection.getKeys(false)) {
            configurationSections.add(configurationSection.getConfigurationSection(ip));
        }

        return configurationSections;
    }

    public static void addDefaultObject(String path, Object object) {
        defaultObjects.put(path, object);
    }

    public static void createPlayerData(UUID uuid) {
        String uuidString = "Players." + uuid.toString();

        if(!FILE.getConfig().contains(uuidString)) FILE.getConfig().createSection(uuidString);

        if(!defaultObjects.isEmpty()) {
            for(String path : defaultObjects.keySet()) {
                String fullPath = uuidString + "." + path;

                if(FILE.getConfig().contains(fullPath)) continue;

                FILE.getConfig().addDefault(fullPath, defaultObjects.get(path));
            }

            FILE.getConfig().options().copyDefaults(true);
        }

        FILE.saveFile(FILE.getConfig());
    }

    public static void createIPData(Player player) {
        String ipString = "IPs." + player.getAddress().getAddress().toString();

        if(!FILE.getConfig().contains(ipString)) FILE.getConfig().createSection(ipString);

        FILE.saveFile(FILE.getConfig());
    }

    public static ConfigurationSection getPlayerData(UUID uuid) {
        return FILE.getConfig().getConfigurationSection("Players." + uuid.toString());
    }

    public static ConfigurationSection getIPData(Player player) {
        return FILE.getConfig().getConfigurationSection("IPs." + player.getAddress().getAddress().toString());
    }

    public static void setPlayerData(UUID uuid, String path, Object object) {
        FILE.getConfig().set("Players." + uuid.toString() + "." + path, object);
        FILE.saveFile(FILE.getConfig());
    }

    public static void setIPData(Player player, String path, Object object) {
        FILE.getConfig().set("IPs." + player.getAddress().getAddress().toString() + "." + path, object);
        FILE.saveFile(FILE.getConfig());
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        createPlayerData(player.getUniqueId());
        createIPData(player);
    }

}
