package net.aminecraftdev.utils.inventory;

import net.aminecraftdev.utils.NumberUtils;
import net.aminecraftdev.utils.inventory.base.ClickAction;
import net.aminecraftdev.utils.itemstack.ItemStackUtils;
import net.aminecraftdev.utils.message.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 18-Oct-17
 *
 * TO-DO: Convert all InventoryBuilder to PanelBuilder
 */
public class PanelBuilder {

    private ConfigurationSection configurationSection;
    private Map<String, String> replaceMap = new HashMap<>();
    private Map<Integer, Integer> pageData = new HashMap<>();
    private Map<String, Set<Integer>> slotsWith = new HashMap<>();
    private Map<String, Map<Integer, Object>> specialValues = new HashMap<>();
    private Map<String, ClickAction> clickActions = new HashMap<>();
    private Map<String, ItemStack> itemStacks = new HashMap<>();
    private Set<Integer> defaultSlots = new HashSet<>();

    private Inventory inventory;
    private int size = 0;

    public PanelBuilder(ConfigurationSection configurationSection) {
        this(configurationSection, null);
    }

    public PanelBuilder(ConfigurationSection configurationSection, Map<String, String> replaceMap) {
        this.configurationSection = configurationSection;

        if(replaceMap != null) this.replaceMap.putAll(replaceMap);
    }

    public PanelBuilder addSpecialCounter(String identifier) {
        specialValues.put(identifier, new HashMap<>());
        return this;
    }

    public PanelBuilder addSpecialCounter(String identifier, ClickAction clickAction) {
        specialValues.put(identifier, new HashMap<>());
        clickActions.put(identifier, clickAction);
        return this;
    }

    public PanelBuilder addSlotCounter(String identifier) {
        slotsWith.put(identifier, new HashSet<>());
        return this;
    }

    public PanelBuilder addSlotCounter(String identifier, ClickAction clickAction) {
        slotsWith.put(identifier, new HashSet<>());
        clickActions.put(identifier, clickAction);
        return this;
    }

    public PanelBuilder addSlotCounter(String identifier, ItemStack itemStack) {
        slotsWith.put(identifier, new HashSet<>());
        itemStacks.put(identifier, itemStack);
        return this;
    }

    public PanelBuilder addSlotCounter(String identifier, ItemStack itemStack, ClickAction clickAction) {
        slotsWith.put(identifier, new HashSet<>());
        itemStacks.put(identifier, itemStack);
        clickActions.put(identifier, clickAction);
        return this;
    }

    public PanelBuilder setSize(int size) {
        this.size = size;
        return this;
    }

    public Set<Integer> getSlotsWith(String identifier) {
        return slotsWith.getOrDefault(identifier, new HashSet<>());
    }

    public Map<Integer, Object> getSpecialSlotsWith(String identifier) {
        return specialValues.getOrDefault(identifier, new HashMap<>());
    }

    public boolean isDefaultSlot(int slot) {
        return defaultSlots.contains(slot);
    }

    public Panel getPanel() {
        build();

        Panel panel = new Panel(this.inventory, this.pageData);

        slotsWith.forEach((identifier, slotsWith) -> {
            if(itemStacks.containsKey(identifier)) {
                slotsWith.forEach(slot -> panel.setItem(slot, itemStacks.get(identifier)));
            }
            if(clickActions.containsKey(identifier)) {
                slotsWith.forEach(slot -> panel.setOnClick(slot, clickActions.get(identifier)));
            }
        });

        return panel;
    }

    private void build() {
        String name = configurationSection.contains("name")? MessageUtils.translateString(configurationSection.getString("name")) : "naming convention error";
        int slots = this.size != 0? this.size : configurationSection.contains("slots")? configurationSection.getInt("slots") : 9;
        ConfigurationSection itemSection = configurationSection.contains("Items")? configurationSection.getConfigurationSection("Items") : null;

        name = replace(name);
        this.inventory = Bukkit.createInventory(null, slots, name);

        if(itemSection != null) {
            for(String s : itemSection.getKeys(false)) {
                int slot = NumberUtils.isStringInteger(s)? Integer.valueOf(s) - 1 : 0;
                ConfigurationSection innerSection = itemSection.getConfigurationSection(s);

                if(innerSection.contains("NextPage") & innerSection.getBoolean("NextPage")) pageData.put(slot, 1);
                if(innerSection.contains("PreviousPage") && innerSection.getBoolean("PreviousPage")) pageData.put(slot, -1);

                for(String identifier : slotsWith.keySet()) {
                    if(innerSection.contains(identifier) && innerSection.getBoolean(identifier)) {
                        Set<Integer> current = slotsWith.get(identifier);

                        current.add(slot);
                        slotsWith.put(identifier, current);
                    }
                }

                for(String identifier : specialValues.keySet()) {
                    if(innerSection.contains(identifier)) {
                        Map<Integer, Object> current = specialValues.get(identifier);

                        current.put(slot, innerSection.get(identifier));
                        specialValues.put(identifier, current);
                    }
                }

                if(slot > inventory.getSize() - 1) continue;

                this.defaultSlots.add(slot);

                if(innerSection.contains("Item")) innerSection = innerSection.getConfigurationSection("Item");
                if(!innerSection.contains("type")) continue;

                this.inventory.setItem(slot, ItemStackUtils.createItemStack(innerSection, 1, replaceMap));
            }
        }
    }

    private String replace(String input) {
        for(Map.Entry<String, String> entry : replaceMap.entrySet()) {
            if(input.contains(entry.getKey())) {
                input = input.replace(entry.getKey(), entry.getValue());
            }
        }

        return input;
    }
}
