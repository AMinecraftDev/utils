package net.aminecraftdev.utils;

import org.bukkit.Location;
import org.bukkit.World;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by charl on 28-Apr-17.
 */
public class StringUtils {

    public static final List<String> breakUp(String input, int length) {
        List<String> temp = new ArrayList<>();
        int i = 0;
        String s = "";
        for(String str : input.split(" ")) {
            if(i + str.toCharArray().length >= length) {
                temp.add(s);
                s = str + " ";
                i = 0;
                continue;
            }

            i += str.toCharArray().length;
            s += str + " ";
        }
        temp.add(s);
        return temp;
    }

    public static List<String> breakUp(String input) {
        List<String> temp = new ArrayList<>();
        for(String str : input.split("\n")) {
            temp.add(str);
        }
        return temp;
    }

    public static String getLocationString(Location location) {
        int x = location.getBlockX();
        int y = location.getBlockY();
        int z = location.getBlockZ();
        World world = location.getWorld();

        return world.getName() + ": " + x + ", " + y + ", " + z;
    }

    public static String formatString(String string) {
        string = string.toLowerCase();

        String s = "";

        if(string.contains(" ")) {
            for(String z : string.split(" ")) {
                s += Character.toUpperCase(z.charAt(0)) + z.substring(1).toLowerCase();
            }
        } else if(string.contains("_")) {
            String[] split = string.split("_");

            for(int i = 0; i < split.length; i++) {
                String z = split[i];

                s += Character.toUpperCase(z.charAt(0)) + z.substring(1).toLowerCase();

                if(i != (split.length - 1)) {
                    s += "_";
                }
            }
        } else {
            return Character.toUpperCase(string.charAt(0)) + string.substring(1).toLowerCase();
        }

        return s;
    }

}
