package net.aminecraftdev.utils.google.youtube.requester;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import net.aminecraftdev.utils.Request;
import net.aminecraftdev.utils.google.youtube.YouTubeWebRequest;
import net.aminecraftdev.utils.google.youtube.objects.ChannelIdentity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public class UploadPlaylistIDRequest implements Request<String> {

    private List<String> result = new ArrayList<>();

    public UploadPlaylistIDRequest(ChannelIdentity channelIdentity) {
        YouTubeWebRequest youTubeWebRequest = new YouTubeWebRequest(YouTubeWebRequest.BASE_URL + "contentDetails");

        youTubeWebRequest.addIdentity(channelIdentity);
        youTubeWebRequest.request();

        JsonArray jsonArray = youTubeWebRequest.getJsonObject().getAsJsonArray("items");

        for(JsonElement jsonElement : jsonArray) {
            String string = jsonElement.getAsJsonObject().getAsJsonObject("contentDetails").getAsJsonObject("relatedPlaylists").get("uploads").getAsString();

            this.result.add(string);
        }
    }

    @Override
    public List<String> get() {
        return this.result;
    }
}
