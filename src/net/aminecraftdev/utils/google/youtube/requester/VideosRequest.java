package net.aminecraftdev.utils.google.youtube.requester;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.aminecraftdev.utils.Request;
import net.aminecraftdev.utils.google.youtube.YouTubeWebRequest;
import net.aminecraftdev.utils.google.youtube.objects.Video;

import java.util.ArrayList;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public class VideosRequest implements Request<Video> {

    private List<Video> result = new ArrayList<>();

    public VideosRequest(String string) {
        YouTubeWebRequest youTubeWebRequest = new YouTubeWebRequest("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=" + string);

        youTubeWebRequest.request();

        JsonArray jsonArray = youTubeWebRequest.getJsonObject().getAsJsonArray("items");

        for(JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject = jsonElement.getAsJsonObject().getAsJsonObject("snippet");
            if (jsonObject != null) {
                String channelId = jsonObject.get("channelId").getAsString();
                String title = jsonObject.get("title").getAsString();
                String description = jsonObject.get("description").getAsString();
                String publishedAt = jsonObject.get("publishedAt").getAsString();
                int position = jsonObject.get("position").getAsInt();
                String videoId = jsonObject.getAsJsonObject("resourceId").get("videoId").getAsString();

                Video video = new Video(channelId, videoId, title, description, position, publishedAt);
                this.result.add(video);
            }
        }
    }

    @Override
    public List<Video> get() {
        return this.result;
    }
}
