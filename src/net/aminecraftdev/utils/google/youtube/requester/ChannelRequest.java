package net.aminecraftdev.utils.google.youtube.requester;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.aminecraftdev.utils.Request;
import net.aminecraftdev.utils.ServerUtils;
import net.aminecraftdev.utils.google.youtube.YouTubeWebRequest;
import net.aminecraftdev.utils.google.youtube.objects.Channel;
import net.aminecraftdev.utils.google.youtube.objects.ChannelIdentity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public class ChannelRequest implements Request<Channel> {

    private List<Channel> result = new ArrayList<>();

    public ChannelRequest(ChannelIdentity channelIdentity) {
        YouTubeWebRequest youTubeWebRequest = new YouTubeWebRequest(YouTubeWebRequest.BASE_URL + "brandingSettings");

        youTubeWebRequest.addIdentity(channelIdentity);

        youTubeWebRequest.request();

        JsonArray jsonArray = youTubeWebRequest.getJsonObject().getAsJsonArray("items");

        for(JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject1 = jsonElement.getAsJsonObject();
            JsonObject jsonObject2 = jsonObject1.getAsJsonObject("brandingSettings").getAsJsonObject("channel");

            String id = jsonObject1.get("id").getAsString();
            String title = jsonObject2.get("title").getAsString();
            String description = "";
            String keywords = "";

            if(jsonObject2.get("description") != null) {
                description = jsonObject2.get("description").getAsString();
            }
            if(jsonObject2.get("keywords") != null) {
                keywords = jsonObject2.get("keywords").getAsString();
            }

            Channel channel = new Channel(channelIdentity, id, title, description, keywords);
            this.result.add(channel);
        }
    }

    @Override
    public List<Channel> get() {
        return this.result;
    }
}
