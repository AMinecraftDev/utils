package net.aminecraftdev.utils.google.youtube.requester;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.aminecraftdev.utils.Request;
import net.aminecraftdev.utils.google.youtube.YouTubeWebRequest;
import net.aminecraftdev.utils.google.youtube.objects.ChannelIdentity;
import net.aminecraftdev.utils.google.youtube.objects.Statistic;

import java.util.ArrayList;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public class StatisticRequest implements Request<Statistic> {

    private List<Statistic> result = new ArrayList<>();

    public StatisticRequest(ChannelIdentity channelIdentity) {
        YouTubeWebRequest youTubeWebRequest = new YouTubeWebRequest(YouTubeWebRequest.BASE_URL + "statistics");

        youTubeWebRequest.addIdentity(channelIdentity);
        youTubeWebRequest.request();

        JsonArray jsonArray = youTubeWebRequest.getJsonObject().getAsJsonArray("items");

        for(JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject1 = jsonElement.getAsJsonObject();
            JsonObject jsonObject2 = jsonObject1.getAsJsonObject("statistics");

            String id = jsonObject1.get("id").getAsString();
            int viewCount = jsonObject2.get("viewCount").getAsInt();
            int commentCount = jsonObject2.get("commentCount").getAsInt();
            int subscriberCount = jsonObject2.get("subscriberCount").getAsInt();
            int videoCount = jsonObject2.get("videoCount").getAsInt();
            boolean hiddenSubscriberCount = jsonObject2.get("hiddenSubscriberCount").getAsBoolean();

            Statistic statistic = new Statistic(channelIdentity, id, viewCount, commentCount, subscriberCount, hiddenSubscriberCount, videoCount);
            this.result.add(statistic);
        }
    }

    @Override
    public List<Statistic> get() {
        return this.result;
    }
}
