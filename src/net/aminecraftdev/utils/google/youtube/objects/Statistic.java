package net.aminecraftdev.utils.google.youtube.objects;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public class Statistic {

    private int viewCount, commentCount, subscriberCount, videoCount;
    private ChannelIdentity channelIdentity;
    private boolean hiddenSubscriberCount;
    private String id;

    public Statistic(ChannelIdentity channelIdentity, String id, int viewCount, int commentCount, int subscriberCount, boolean hiddenSubscriberCount, int videoCount) {
        this.channelIdentity = channelIdentity;
        this.id = id;
        this.viewCount = viewCount;
        this.commentCount = commentCount;
        this.subscriberCount = subscriberCount;
        this.hiddenSubscriberCount = hiddenSubscriberCount;
        this.videoCount = videoCount;
    }

    public ChannelIdentity getChannelIdentity() {
        return channelIdentity;
    }

    public String getId() {
        return id;
    }

    public int getViewCount() {
        return viewCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public int getSubscriberCount() {
        return subscriberCount;
    }

    public boolean isHiddenSubscriberCount() {
        return hiddenSubscriberCount;
    }

    public int getVideoCount() {
        return videoCount;
    }
}
