package net.aminecraftdev.utils.google.youtube.objects;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public class Channel {

    private ChannelIdentity channelIdentify;
    private String id, title, description, keywords;

    public Channel(ChannelIdentity channelIdentify, String id, String title, String description, String keywords) {
        this.channelIdentify = channelIdentify;
        this.id = id;
        this.title = title;
        this.description = description;
        this.keywords = keywords;
    }

    public ChannelIdentity getChannelIdentify() {
        return channelIdentify;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getKeywords() {
        return keywords;
    }
}
