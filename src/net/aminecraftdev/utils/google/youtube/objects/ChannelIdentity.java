package net.aminecraftdev.utils.google.youtube.objects;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public class ChannelIdentity {

    private String id, name;

    public ChannelIdentity(boolean bool, String string) {
        if(bool) {
            this.id = string;
        } else {
            this.name = string;
        }
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLinkPart() {
        if(this.id != null) {
            return "&id=" + this.id;
        } else {
            return "&forUsername=" + this.name;
        }
    }

    public String getURL() {
        String string = "https://www.youtube.com";

        if(this.id != null) {
            string += "/channel/" + this.id;
        } else {
            string += "/user/" + this.name;
        }

        return string;
    }

    public static ChannelIdentity fromUsername(String username) {
        return new ChannelIdentity(false, username);
    }

    public static ChannelIdentity fromID(String id) {
        return new ChannelIdentity(true, id);
    }

    public static ChannelIdentity fromURL(String url) {
        if(!isChannelURL(url)) return null;

        String[] array = url.split("/");
        String string = array[(array.length - 1)];

        if(url.contains("/user/")) return new ChannelIdentity(false, string);
        if(url.contains("/channel/")) return new ChannelIdentity(true, string);
        return null;
    }

    private static boolean isChannelURL(String url) {
        if(url == null || url.equals("") || url.endsWith("/")) return false;
        if((!url.startsWith("https://")) & (!url.startsWith("https://"))) return false;

        url = url.replace(" ", "");

        int i = 0;

        for(int j = 0; j < url.length(); j++) {
            if(url.charAt(j) == '/') i++;
        }

        return !((i != 4) || (!url.contains("youtube.com/")));
    }
}
