package net.aminecraftdev.utils.google.youtube.objects;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public class Video {

    private String channelId, videoId, title, description, publishedAt;
    private int position;

    public Video(String channelId, String videoId, String title, String description, int position, String publishedAt) {
        this.channelId = channelId;
        this.videoId = videoId;
        this.title = title;
        this.description = description;
        this.position = position;
        this.publishedAt = publishedAt;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public int getPosition() {
        return position;
    }

    public String getChannelId() {
        return channelId;
    }

    public String getVideoId() {
        return videoId;
    }

    public String getURL() {
        return "https://www.youtube.com/watch?v=" + this.videoId;
    }

    public String getPublishedAt() {
        return this.publishedAt;
    }
}
