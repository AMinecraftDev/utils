package net.aminecraftdev.utils.google.youtube;

import net.aminecraftdev.utils.IModule;
import net.aminecraftdev.utils.SectionModule;
import net.aminecraftdev.utils.qplugin.QPlugin;
import net.aminecraftdev.utils.google.youtube.caches.*;
import net.aminecraftdev.utils.google.youtube.objects.Channel;
import net.aminecraftdev.utils.google.youtube.objects.ChannelIdentity;
import net.aminecraftdev.utils.google.youtube.objects.Statistic;
import net.aminecraftdev.utils.google.youtube.objects.Video;

import java.util.ArrayList;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public class YouTube implements IModule {

    private static YouTube INSTANCE;

    private ChannelCache channelCache;
    private StatisticCache statisticCache;
    private UploadIDCache uploadIDCache;
    private VideoCache videoCache;
    private PlaylistCache playlistCache;

    public YouTube() {
        INSTANCE = this;
    }

    @Override
    public void enable() {
        this.channelCache = new ChannelCache();
        this.statisticCache = new StatisticCache();
        this.uploadIDCache = new UploadIDCache();
        this.videoCache = new VideoCache();
        this.playlistCache = new PlaylistCache();
    }

    @Override
    public void disable() {

    }

    @Override
    public String getModuleName() {
        return "YouTube";
    }

    public String getUploadID(ChannelIdentity channelIdentity) {
        return this.uploadIDCache.get(channelIdentity, true);
    }

    public Channel getChannel(ChannelIdentity channelIdentity, boolean bool) {
        if(channelIdentity == null) return null;

        return this.channelCache.get(channelIdentity, bool);
    }

    public Statistic getStatistic(ChannelIdentity channelIdentity, boolean bool) {
        if(channelIdentity == null) return null;

        return this.statisticCache.get(channelIdentity, bool);
    }

    public List<Video> getUploads(ChannelIdentity channelIdentity, boolean bool) {
        if(channelIdentity == null) return null;

        List<Video> videoList = this.videoCache.get(channelIdentity, bool);

        if(videoList == null) videoList = new ArrayList<>();

        return videoList;
    }

    public List<Video> getPlaylist(String playlistId, boolean bool) {
        if(playlistId == null || playlistId.equals("")) return null;

        List<Video> videoList = this.playlistCache.get(playlistId, bool);

        if(videoList == null) videoList = new ArrayList<>();

        return videoList;
    }

    public static YouTube get() {
        return INSTANCE;
    }

}
