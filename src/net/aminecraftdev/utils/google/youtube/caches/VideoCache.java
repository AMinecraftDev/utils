package net.aminecraftdev.utils.google.youtube.caches;

import net.aminecraftdev.utils.google.youtube.YouTube;
import net.aminecraftdev.utils.google.youtube.caches.sample.IdentityCache;
import net.aminecraftdev.utils.google.youtube.objects.ChannelIdentity;
import net.aminecraftdev.utils.google.youtube.objects.Video;
import net.aminecraftdev.utils.google.youtube.requester.VideosRequest;

import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public class VideoCache extends IdentityCache<List<Video>> {

    @Override
    public List<Video> fetch(ChannelIdentity channelIdentity) {
        String string = YouTube.get().getUploadID(channelIdentity);
        VideosRequest videosRequest = new VideosRequest(string);

        if(!videosRequest.get().isEmpty()) return videosRequest.get();

        return null;
    }
}
