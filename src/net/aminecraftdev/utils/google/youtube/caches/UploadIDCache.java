package net.aminecraftdev.utils.google.youtube.caches;

import net.aminecraftdev.utils.google.youtube.caches.sample.IdentityCache;
import net.aminecraftdev.utils.google.youtube.objects.ChannelIdentity;
import net.aminecraftdev.utils.google.youtube.requester.UploadPlaylistIDRequest;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public class UploadIDCache extends IdentityCache<String> {

    @Override
    public String fetch(ChannelIdentity channelIdentity) {
        UploadPlaylistIDRequest uploadPlaylistIDRequest = new UploadPlaylistIDRequest(channelIdentity);

        if(!uploadPlaylistIDRequest.get().isEmpty()) return uploadPlaylistIDRequest.get().get(0);

        return null;
    }
}
