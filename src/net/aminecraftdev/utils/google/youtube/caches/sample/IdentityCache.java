package net.aminecraftdev.utils.google.youtube.caches.sample;

import net.aminecraftdev.utils.google.youtube.objects.ChannelIdentity;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public abstract class IdentityCache<OBJ> extends DoubleCache<ChannelIdentity, OBJ> {
}
