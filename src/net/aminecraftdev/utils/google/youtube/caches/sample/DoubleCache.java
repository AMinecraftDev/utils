package net.aminecraftdev.utils.google.youtube.caches.sample;

import java.util.HashMap;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public abstract class DoubleCache<Key, Value> implements IDoubleCacheFetcher<Key, Value> {

    private Map<Key, Value> cache = new HashMap<>();

    public Value get(Key key, boolean bool) {
        if((this.cache.containsKey(key)) && (bool)) {
            return this.cache.get(key);
        }

        Value object = fetch(key);

        if(object != null) {
            this.cache.put(key, object);
        }

        return object;
    }
}
