package net.aminecraftdev.utils.google.youtube.caches.sample;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public interface IDoubleCacheFetcher<Key, Value> {

    Value fetch(Key key);

}
