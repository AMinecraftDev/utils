package net.aminecraftdev.utils.google.youtube.caches;

import net.aminecraftdev.utils.google.youtube.caches.sample.IdentityCache;
import net.aminecraftdev.utils.google.youtube.objects.Channel;
import net.aminecraftdev.utils.google.youtube.objects.ChannelIdentity;
import net.aminecraftdev.utils.google.youtube.requester.ChannelRequest;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public class ChannelCache extends IdentityCache<Channel> {

    @Override
    public Channel fetch(ChannelIdentity channelIdentity) {
        ChannelRequest channelRequest = new ChannelRequest(channelIdentity);

        if(!channelRequest.get().isEmpty()) return channelRequest.get().get(0);


        return null;
    }
}
