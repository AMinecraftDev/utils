package net.aminecraftdev.utils.google.youtube.caches;

import net.aminecraftdev.utils.google.youtube.caches.sample.IdentityCache;
import net.aminecraftdev.utils.google.youtube.objects.ChannelIdentity;
import net.aminecraftdev.utils.google.youtube.objects.Statistic;
import net.aminecraftdev.utils.google.youtube.requester.StatisticRequest;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public class StatisticCache extends IdentityCache<Statistic> {

    @Override
    public Statistic fetch(ChannelIdentity channelIdentity) {
        StatisticRequest statisticRequest = new StatisticRequest(channelIdentity);

        if(!statisticRequest.get().isEmpty()) return statisticRequest.get().get(0);

        return null;
    }
}
