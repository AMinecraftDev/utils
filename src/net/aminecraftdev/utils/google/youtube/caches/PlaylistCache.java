package net.aminecraftdev.utils.google.youtube.caches;

import net.aminecraftdev.utils.google.youtube.caches.sample.StringCache;
import net.aminecraftdev.utils.google.youtube.objects.Video;
import net.aminecraftdev.utils.google.youtube.requester.VideosRequest;

import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 08-Dec-17
 */
public class PlaylistCache extends StringCache<List<Video>> {

    @Override
    public List<Video> fetch(String s) {
        VideosRequest videosRequest = new VideosRequest(s);

        if(!videosRequest.get().isEmpty()) return videosRequest.get();

        return null;
    }
}
