package net.aminecraftdev.utils.google.youtube;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.aminecraftdev.utils.google.IWebRequest;
import net.aminecraftdev.utils.google.youtube.objects.ChannelIdentity;
import org.bukkit.Bukkit;
import org.jsoup.Jsoup;

import java.io.IOException;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public class YouTubeWebRequest implements IWebRequest<ChannelIdentity> {

    public static String BASE_URL = "https://www.googleapis.com/youtube/v3/channels?part=";

    private String url;
    private String rawJson;
    private JsonObject jsonObject;

    public YouTubeWebRequest(String string) {
        this.url = (string + "&key=AIzaSyBMse_LTm96sp3kD3tNdMeu2CF78AJeZjw");
    }

    @Override
    public String getURL() {
        return url;
    }

    @Override
    public JsonObject getJsonObject() {
        return this.jsonObject;
    }

    @Override
    public String getRawJson() {
        return this.rawJson;
    }

    @Override
    public void request() {
        try {
            this.rawJson = Jsoup.connect(this.url).ignoreContentType(true).execute().body();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        this.jsonObject = new JsonParser().parse(this.rawJson).getAsJsonObject();
    }

    @Override
    public void addIdentity(ChannelIdentity channelIdentity) {
        if(channelIdentity == null) {
            Bukkit.getLogger().warning("YouTubeWebRequest error for " + this.url);
            return;
        }

        this.url += channelIdentity.getLinkPart();
    }
}
