package net.aminecraftdev.utils.google.urlshortener;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.aminecraftdev.utils.google.IWebRequest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 13-Dec-17
 */
public class UrlShortenerWebRequest implements IWebRequest<String> {

    public static String BASE_URL = "https://www.googleapis.com/urlshortener/v1/url";

    private String rawJson, url, longUrl;
    private JsonObject jsonObject;

    public UrlShortenerWebRequest(String string) {
        this.url = (string + "&key=AIzaSyBMse_LTm96sp3kD3tNdMeu2CF78AJeZjw");
    }

    @Override
    public String getURL() {
        return this.url;
    }

    @Override
    public JsonObject getJsonObject() {
        return this.jsonObject;
    }

    @Override
    public String getRawJson() {
        return this.rawJson;
    }

    @Override
    public void request() {
        try {
            ProcessBuilder processBuilder = new ProcessBuilder(
                    "curl " + this.url + "\\",
                    "-H 'Content-Type: application/json' \\",
                    "-d '{\"longUrl\": \"" + this.longUrl + "\"}'");
            Process process = processBuilder.start();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line;

            while((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(System.getProperty("line.separator"));
            }

            try {
                process.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }

            this.rawJson = stringBuilder.toString();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        this.jsonObject = new JsonParser().parse(this.rawJson).getAsJsonObject();
    }

    @Override
    public void addIdentity(String longUrl) {
        if(longUrl == null || longUrl.isEmpty()) return;

        this.longUrl = longUrl;
    }
}
