package net.aminecraftdev.utils.google;

import com.google.gson.JsonObject;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 13-Dec-17
 */
public interface IWebRequest<IdentityType> {

    String getURL();

    JsonObject getJsonObject();

    String getRawJson();

    void request();

    void addIdentity(IdentityType identityType);

}
