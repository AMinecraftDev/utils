package net.aminecraftdev.utils;

import net.aminecraftdev.utils.command.builder.CommandService;
import net.aminecraftdev.utils.file.YmlFile;
import net.aminecraftdev.utils.qplugin.QPlugin;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 03-Jul-17
 */
public abstract class SubPlugin implements Reloadable {

    protected static QPlugin PLUGIN;

    private List<PluginMessageListener> messageListeners = new ArrayList<>();
    private List<CommandService> commands = new ArrayList<>();
    private List<Listener> listeners = new ArrayList<>();
    private Set<YmlFile> files = new HashSet<>();
    private boolean ownFile;
    private File folder;

    public SubPlugin(boolean ownFile) {
        this.ownFile = ownFile;

        if(this.ownFile) {
            this.folder = new File(PLUGIN.getDataFolder(), this.getClass().getName());
        }
    }

    protected abstract boolean onEnable();
    protected abstract void onDisable();

    public void addListener(Listener listener) {
        this.listeners.add(listener);
        ServerUtils.registerListener(listener, PLUGIN);
    }

    public void addCommand(CommandService service) {
        this.commands.add(service);
    }

    public void addMessageListener(PluginMessageListener pluginMessageListener) {
        this.messageListeners.add(pluginMessageListener);
        PLUGIN.getServer().getMessenger().registerIncomingPluginChannel(PLUGIN, "BungeeCord", pluginMessageListener);
    }

    public void addFile(YmlFile ymlFile) {
        this.files.add(ymlFile);
    }

    public File getFolder() {
        return this.folder;
    }

    public boolean enable() {
        if(this.ownFile && this.folder != null && !this.folder.exists()) {
            this.folder.mkdir();
        }

        return onEnable();
    }

    public void disable() {
        for(Listener listener : this.listeners) {
            HandlerList.unregisterAll(listener);
        }

        onDisable();
    }

    public void saveFiles() {
        this.files.forEach(file -> file.saveFile(file.getConfig()));
    }

    public void reloadFiles() {
        this.files.forEach(YmlFile::loadFile);
    }

    /*  Static methods for outside main
        sub-plugin usage.
     */

    public static QPlugin getPlugin() {
        return PLUGIN;
    }

    public static void setPlugin(QPlugin newPlugin) {
        PLUGIN = newPlugin;
    }

}
