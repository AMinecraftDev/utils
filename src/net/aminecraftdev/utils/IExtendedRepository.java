package net.aminecraftdev.utils;

import java.sql.Connection;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 08-Dec-17
 */
public interface IExtendedRepository<TType, TUnique> extends IRepository<TType, TUnique> {

    Map<TType, TUnique> ReadAll(); //Get the entire repository.

    Connection getConnection();

}
