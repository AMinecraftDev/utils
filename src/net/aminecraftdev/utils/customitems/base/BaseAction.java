package net.aminecraftdev.utils.customitems.base;

import org.bukkit.event.Event;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Jan-18
 */
public interface BaseAction<T extends Event> {

    boolean onAction(T event);

}
