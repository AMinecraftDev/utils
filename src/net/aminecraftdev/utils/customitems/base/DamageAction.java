package net.aminecraftdev.utils.customitems.base;

import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Jan-18
 */
public interface DamageAction extends BaseAction<EntityDamageByEntityEvent> {

}
