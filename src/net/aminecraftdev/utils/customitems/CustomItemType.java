package net.aminecraftdev.utils.customitems;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Jan-18
 */
public enum CustomItemType {

    VOUCHER,
    WEAPON,
    TOOL,
    OTHER

}
