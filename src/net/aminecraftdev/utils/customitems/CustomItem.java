package net.aminecraftdev.utils.customitems;

import net.aminecraftdev.utils.PluginUtils;
import net.aminecraftdev.utils.ServerUtils;
import net.aminecraftdev.utils.customitems.base.BaseAction;
import net.aminecraftdev.utils.factory.NbtFactory;
import net.aminecraftdev.utils.qplugin.QPlugin;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Jan-18
 */
public class CustomItem<T extends BaseAction> {

    private static List<CustomItem> CUSTOM_ITEMS = new ArrayList<>();
    private static boolean REGISTERED = false;
    private static QPlugin PLUGIN;

    private boolean takeItemWhenDone = false, checkIfSimilar = true;
    private List<String> compoundKeys = new ArrayList<>();
    private List<T> actions = new ArrayList<>();
    private Sound successfulActivationSound;
    private CustomItemType customItemType;
    private ItemStack baseCustomItem;

    public CustomItem(ItemStack itemStack) {
        this(CustomItemType.OTHER, itemStack, null, null);
    }

    public CustomItem(CustomItemType customItemType, ItemStack itemStack, Sound redeemSound) {
        this(customItemType, itemStack, null, redeemSound);
    }

    public CustomItem(CustomItemType customItemType, ItemStack itemStack, List<String> compoundKeys) {
        this(customItemType, itemStack, compoundKeys, null);
    }

    public CustomItem(CustomItemType customItemType, ItemStack itemStack, List<String> compoundKeys, Sound successfulActivationSound) {
        this.customItemType = customItemType;
        this.baseCustomItem = itemStack;
        this.compoundKeys = compoundKeys;
        this.successfulActivationSound = successfulActivationSound;

        if(!REGISTERED) {
            ServerUtils.registerListener(getListeners(), PLUGIN);
            REGISTERED = true;
        }

        CUSTOM_ITEMS.add(this);
    }

    public ItemStack getBaseItem() {
        return this.baseCustomItem;
    }

    public boolean isCheckIfSimilar() {
        return this.checkIfSimilar;
    }

    public List<String> getCompoundKeys() {
        return this.compoundKeys;
    }

    private Listener getListeners() {
        return new Listener() {
            @EventHandler
            public void onClick(PlayerInteractEvent event) {
                Player player = event.getPlayer();
                ItemStack itemStack = PluginUtils.getItemInHand(player);
                ItemStack cloneStack = itemStack.clone();

                if(!event.getAction().toString().startsWith("RIGHT_CLICK") || CustomItem.CUSTOM_ITEMS.isEmpty() ||
                        cloneStack == null || cloneStack.getType() == Material.AIR) return;

                cloneStack.setAmount(1);

                for(CustomItem customItem : new ArrayList<>(CustomItem.CUSTOM_ITEMS)) {
                    if(customItem == null || customItem.getBaseItem() == null) continue;
                    if(customItem.isCheckIfSimilar()) if(!cloneStack.isSimilar(customItem.getBaseItem())) continue;

                    if(customItem.getCompoundKeys() != null && !customItem.getCompoundKeys().isEmpty()) {
                        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(NbtFactory.getCraftItemStack(itemStack));
                        boolean containsAllPaths = false;

                        if(compound == null || compound.isEmpty()) continue;

                        //TO-DO: FINISH
                    }
                }
            }
        };
    }

}
