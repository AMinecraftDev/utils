package net.aminecraftdev.utils.file;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by charl on 07-May-17.
 */
public class FileUtils {

    public static void saveFile(File file, YamlConfiguration configuration) {
        try {
            configuration.save(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static YamlConfiguration getConf(File file) {
        return YamlConfiguration.loadConfiguration(file);
    }

    public static void moveFile(File file, File destination) {
        Path source = Paths.get(file.getPath());

        if(!file.exists()) return;
        if(destination.exists()) return;

        try {
            Files.move(source, source.resolveSibling(destination.getName()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
