package net.aminecraftdev.utils.file;

import net.aminecraftdev.utils.builders.IBuilder;
import net.aminecraftdev.utils.module.ModuleManager;
import net.aminecraftdev.utils.qplugin.QPlugin;

import java.io.File;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 06-Feb-18
 */
public class FileManagement implements IBuilder<File> {

    private static QPlugin PLUGIN = ModuleManager.getPlugin();

    private File oldFile, newFile;
    private boolean saveResource;

    public FileManagement(String beginning, File normal, boolean saveResource) {
        this.newFile = normal;
        this.oldFile = new File(normal.getParent(), beginning + normal.getName());
        this.saveResource = saveResource;
    }

    @Override
    public File build() {
        if(this.saveResource) {
            if(!this.oldFile.exists()) {
                PLUGIN.saveResource(this.oldFile.getName(), false);
            }
        }

        FileUtils.moveFile(this.oldFile, this.newFile);

        return this.newFile;
    }
}
