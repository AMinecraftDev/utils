package net.aminecraftdev.utils.file;

import net.aminecraftdev.utils.IFile;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 05-Feb-18
 */
public class YmlFile implements IFile<YamlConfiguration, YamlConfiguration> {

    private static Map<String, YmlFile> FILES = new HashMap<>();

    private YamlConfiguration fileConfiguration;
    private File file;

    public YmlFile(File file) {
        this.file = file;

        loadFile();
        FILES.put(getKey(), this);
    }

    @Override
    public void saveFile(YamlConfiguration yamlConfiguration) {
        FileUtils.saveFile(this.file, yamlConfiguration);
    }

    @Override
    public YamlConfiguration loadFile() {
        this.fileConfiguration = FileUtils.getConf(this.file);
        return this.fileConfiguration;
    }

    @Override
    public YamlConfiguration getConfig() {
        return this.fileConfiguration;
    }

    @Override
    public void delete() {
        FILES.remove(getKey());
        this.file.delete();
    }

    @Override
    public void rename(File file) {
        String oldKey = getKey();
        String newKey = getKey(file);

        if(!file.exists()) {
            FILES.remove(oldKey);
            FILES.put(newKey, this);

            FileUtils.moveFile(this.file, file);
            this.file = file;
            loadFile();
        }
    }

    @Override
    public String getKey() {
        return getKey(this.file);
    }

    public static String getKey(File file) {
        return file.getAbsolutePath();
    }

    public static YmlFile getFile(String path) {
        return FILES.getOrDefault(path, null);
    }
}
