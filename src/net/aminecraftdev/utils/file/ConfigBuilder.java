package net.aminecraftdev.utils.file;

import net.aminecraftdev.utils.builders.IBuilder;
import net.aminecraftdev.utils.message.MessageUtils;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 07-Feb-18
 */
public class ConfigBuilder implements IBuilder<FileConfiguration> {

    private FileConfiguration configuration;

    public ConfigBuilder(File file) {
        this.configuration = FileUtils.getConf(file);
    }

    public ConfigBuilder addDefault(String path, Object value) {
        this.configuration.addDefault(path, value);
        return this;
    }

    public ConfigBuilder addItemStack(String path, ItemStack itemStack) {
        if(itemStack != null) {
            this.configuration.addDefault(path + ".type", itemStack.getType());

            if(itemStack.getDurability() != 0) {
                this.configuration.addDefault(path + ".durability", itemStack.getDurability());
            }

            if(itemStack.hasItemMeta()) {
                ItemMeta itemMeta = itemStack.getItemMeta();

                if(itemMeta.hasDisplayName()) this.configuration.addDefault(path + " .name", MessageUtils.reverseTranslate(itemMeta.getDisplayName()));
                if(itemMeta.hasLore()) {
                    List<String> current = itemMeta.getLore();
                    List<String> newLore = new ArrayList<>();

                    current.forEach(string -> newLore.add(MessageUtils.translateString(string)));
                    this.configuration.addDefault(path + ".lore", newLore);
                }

                if(itemMeta instanceof SkullMeta) {
                    SkullMeta skullMeta = (SkullMeta) itemMeta;

                    if(skullMeta.hasOwner()) {
                        this.configuration.addDefault(path + ".owner", skullMeta.getOwner());
                    }
                }

                if(!itemStack.getEnchantments().isEmpty() || itemMeta.hasItemFlag(ItemFlag.HIDE_ENCHANTS)) {
                    List<String> enchants = new ArrayList<>();
                    Map<Enchantment, Integer> enchantments = itemStack.getEnchantments();

                    enchantments.forEach((ench, level) -> {
                        enchants.add(ench.getName() + ":" + level);
                    });

                    if(itemMeta.hasItemFlag(ItemFlag.HIDE_ENCHANTS)) {
                        enchants.add("GLOW:1");
                    }

                    this.configuration.addDefault(path + ".enchants", enchants);
                }
            }
        }

        return this;
    }

    public ConfigBuilder addInventory(String path, Inventory inventory, Map<String, Object> itemValues) {
        if(inventory != null) {
            String name = inventory.getName();
            int slots = inventory.getSize();

            this.configuration.addDefault(path + ".name", MessageUtils.reverseTranslate(name));
            this.configuration.addDefault(path + ".slots", slots);

            for(int i = 0; i < inventory.getSize(); i++) {
                ItemStack itemStack = inventory.getItem(i);

                if(itemStack == null || itemStack.getType() == Material.AIR) continue;

                int slot = i+1;
                addItemStack(path + ".Items." + slot, itemStack);
            }
        }

        if(itemValues != null) {
            for(Map.Entry<String, Object> entry : itemValues.entrySet()) {
                String itemPath = entry.getKey();
                Object value = entry.getValue();

                addDefault(path + ".Items." + itemPath, value);
            }
        }

        return this;
    }

    public ConfigBuilder addInventory(String path, Inventory inventory) {
        return addInventory(path, inventory, null);
    }

    @Override
    public FileConfiguration build() {
        this.configuration.options().copyDefaults(true);
        return this.configuration;
    }
}
