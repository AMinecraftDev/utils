package net.aminecraftdev.utils.file;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import net.aminecraftdev.utils.IFile;

import java.io.*;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 05-Feb-18
 */
public class JsonFile<T> implements IFile<T, FileReader> {

    private static Map<String, JsonFile> FILES = new HashMap<>();

    private boolean isArray;
    private Class<T> clazz;
    private Gson gson;
    private File file;

    public JsonFile(File file, boolean isArray, Gson gson) {
        this(file, isArray, gson, null);
    }

    public JsonFile(File file, boolean isArray, Gson gson, Class<T> clazz) {
        this.file = file;
        this.isArray = isArray;
        this.gson = gson;
        this.clazz = clazz;

        loadFile();
        FILES.put(getKey(), this);
    }

    @Override
    public void saveFile(T t) {
        try {
            FileWriter fileWriter = new FileWriter(this.file);
            String json = this.gson.toJson(t);

            fileWriter.write(json);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public T loadFile() {
        FileReader fileReader = getConfig();
        T t;

        if(this.isArray) {
            Type type = new TypeToken<T>(){}.getType();

            t = this.gson.fromJson(fileReader, type);
        } else {
            t = this.gson.fromJson(fileReader, this.clazz);
        }

        return t;
    }

    @Override
    public FileReader getConfig() {
        try {
            return new FileReader(this.file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void delete() {
        FILES.remove(getKey());
        this.file.delete();
    }

    @Override
    public void rename(File file) {
        String oldKey = getKey();
        String newKey = getKey(file);

        if(!file.exists()) {
            FILES.remove(oldKey);
            FILES.put(newKey, this);

            FileUtils.moveFile(this.file, file);
            this.file = file;
            loadFile();
        }
    }

    @Override
    public String getKey() {
        return getKey(this.file);
    }

    private String getKey(File file) {
        return file.getAbsolutePath();
    }

    public static JsonFile getFile(String path) {
        return FILES.getOrDefault(path, null);
    }
}
