package net.aminecraftdev.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 18-Oct-17
 */
public class CollectionUtils {

    public static <V> List<V> shuffleArray(List<V> currentList) {
        Collections.shuffle(currentList);

        return currentList;
    }

    public static <T> List<T> getRandomObjects(List<T> currentList, int amount) {
        return getRandomObjects(currentList, amount, false);
    }

    public static <T> List<T> getRandomObjects(List<T> currentList, int amount, boolean onlyOneEach) {
        List<T> newList = new ArrayList<>();

        while(newList.size() < amount) {
            int random = RandomUtils.getRandomInt(currentList.size());
            T object = currentList.get(random);

            if(onlyOneEach && newList.contains(object)) continue;

            newList.add(object);
        }

        return newList;
    }



}
