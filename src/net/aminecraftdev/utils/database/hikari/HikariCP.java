package net.aminecraftdev.utils.database.hikari;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import net.aminecraftdev.utils.ServerUtils;
import net.aminecraftdev.utils.database.SQLQueries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 23-Jan-18
 */
public class HikariCP {

    private String host, port, username, password, database;
    private HikariDataSource hikariDataSource;

    public HikariCP(String host, String port, String username, String password, String database) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.database = database;

        connect();
    }

    public ResultSet executeQuery(String sql, Object... objects) {
        try(Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            int count = 1;

            for(Object object : objects) {
                preparedStatement.setObject(count, object);
                count++;
            }

            return preparedStatement.executeQuery();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public void executeUpdate(String sql, Object... objects) {
        try(Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            int count = 1;

            for(Object object : objects) {
                preparedStatement.setObject(count, object);
                count++;
            }

            preparedStatement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public ResultSet getValues(String table) {
        return executeQuery("SELECT * FROM " + table);
    }

    public Object getValue(String table, String key, Object placeholder, String column) {
        Object object = null;

        try(ResultSet resultSet = executeQuery("SELECT * FROM " + table + " WHERE " + key + "=?;", placeholder)) {
            if(resultSet.next()) {
                object = resultSet.getObject(column);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return object;
    }

    public void setValues(String table, String primaryKey, Object primaryValue, Map<String, Object> objects) {
        String string = SQLQueries.updateAll(table, primaryKey, primaryValue, objects);

        try {
            executeUpdate(string);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setValue(String table, String primaryKey, Object primaryValue, String targetKey, Object targetValue) {
        String string = SQLQueries.updateOne(table, primaryKey, targetKey);

        try {
            executeUpdate(string, primaryValue, targetValue, targetValue);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void createTable(String table, List<String> column) {
        String createTable = SQLQueries.createTable(table, column);

        ServerUtils.runTaskAsync(() -> executeUpdate(createTable));
    }

    public void deleteEntry(String table, String key, Object value) {
        String delete = SQLQueries.deleteOne(table, key);

        ServerUtils.runTaskAsync(() -> executeUpdate(delete, value));
    }

    public boolean isConnected() {
        return !this.hikariDataSource.isClosed();
    }

    public Connection getConnection() {
        return this.hikariDataSource.getConnection();
    }

    public void disconnect() {
        this.hikariDataSource.close();
    }

    public void restart() {
        disconnect();
        connect();
    }

    private void connect() {
        try {
            HikariConfig hikariConfig = new HikariConfig();

            hikariConfig.setJdbcUrl("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database + "?characterEncoding=utf8");
            hikariConfig.setUsername(this.username);
            hikariConfig.setPassword(this.password);
            hikariConfig.setMinimumIdle(0);
            hikariConfig.setMaximumPoolSize(30);

            hikariConfig.addDataSourceProperty("useSSL", false);
            hikariConfig.addDataSourceProperty("cachePrepStmts", true);
            hikariConfig.addDataSourceProperty("prepStmtCacheSize", 250);
            hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
            hikariConfig.addDataSourceProperty("useServerPrepStmts", true);
            hikariConfig.addDataSourceProperty("useLocalSessionState", true);
            hikariConfig.addDataSourceProperty("useLocalTransactionState", true);
            hikariConfig.addDataSourceProperty("rewriteBatchedStatements", true);
            hikariConfig.addDataSourceProperty("cacheResultSetMetadata", true);
            hikariConfig.addDataSourceProperty("cacheServerConfiguration", true);
            hikariConfig.addDataSourceProperty("elideSetAutoCommits", true);
            hikariConfig.addDataSourceProperty("maintainTimeStats", false);

            this.hikariDataSource = new HikariDataSource(hikariConfig);

            this.hikariDataSource.setIdleTimeout(60000);
            this.hikariDataSource.setConnectionTimeout(60000);
            this.hikariDataSource.setValidationTimeout(3000);
            this.hikariDataSource.setLoginTimeout(5);
            this.hikariDataSource.setMaxLifetime(60000);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
