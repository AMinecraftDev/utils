package net.aminecraftdev.utils.database;

import java.util.*;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 17-Nov-17
 */
public class SQLQueries {

    public static String selectAllOne(String tableName, String row) {
        return "SELECT * FROM " + tableName + " WHERE " + row + "=?;";
    }

    public static String selectAll(String tableName) {
        return "SELECT * FROM " + tableName + ";";
    }

    public static String selectOne(String tableName, String column, String row) {
        return "SELECT " + column + " FROM " + tableName + " WHERE " + row + "=?;";
    }

    public static String updateSingle(String tableName, String key) {
        return "INSERT INTO " + tableName + "(" + key + ") VALUES (?) ON DUPLICATE KEY UPDATE " + key + "=?;";
    }

    public static String updateOne(String tableName, String primaryKey, String column) {
        return "INSERT INTO " + tableName + " (" + primaryKey + ", " + column + ") VALUES (?, ?) ON DUPLICATE KEY UPDATE " + column + "=?;";
    }

    // INSERT INTO `test2` (`id`, `format`, `data`, `randomInfo`) VALUES (2, "Newish", "6 Billion", "Random Info 1.0")
    // ON DUPLICATE KEY UPDATE `format`="Brand New", `data`="8 Billion", `randomInfo`="Random Info 2.0";

//  tableName = test2
//  primaryKey = id
//  columns = Arrays.asList("format", "data", "randomInfo");
//
//    columnBuilder = (id, format, data, randomInfo)
//    valuesBuilder = (?, ?, ?, ?)
//    onDuplicateBuilder = format=?, data=?, randomInfo=?
//
//    return String = "INSERT INTO test2 (id, format, data, randomInfo) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE format=?, data=?, randomInfo=?;
    public static String updateAll(String tableName, String primaryKey, Object primaryValue, Map<String, Object> objects) {
        StringBuilder columnBuilder = new StringBuilder("(");
        StringBuilder valuesBuilder = new StringBuilder("(");
        StringBuilder onDuplicateBuilder = new StringBuilder();

        columnBuilder.append(primaryKey);
        columnBuilder.append(", ");

        checkIfString(primaryValue, valuesBuilder);
        valuesBuilder.append(", ");

        Iterator<Map.Entry<String, Object>> iterator = objects.entrySet().iterator();

        while(iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            String string = entry.getKey();
            Object value = entry.getValue();

            columnBuilder.append("`");
            columnBuilder.append(string);
            columnBuilder.append("`");
            checkIfString(value, valuesBuilder);
            onDuplicateBuilder.append("`");
            onDuplicateBuilder.append(string);
            onDuplicateBuilder.append("`");
            onDuplicateBuilder.append("=");
            checkIfString(value, onDuplicateBuilder);

            if(iterator.hasNext()) {
                columnBuilder.append(", ");
                valuesBuilder.append(", ");
                onDuplicateBuilder.append(", ");
            }
        }

        columnBuilder.append(")");
        valuesBuilder.append(")");

        return "INSERT INTO " + tableName + " " + columnBuilder.toString() + " VALUES " + valuesBuilder.toString() + " ON DUPLICATE KEY UPDATE " + onDuplicateBuilder.toString() + ";";
    }

    public static String deleteOne(String tableName, String column) {
        return "DELETE FROM " + tableName + " WHERE " + column + "=? LIMIT 1;";
    }

    public static String createTable(String tableName, List<String> columns) {
        StringBuilder stringBuilder = new StringBuilder("(");
        Queue<String> queue = new LinkedList<>(columns);

        while(!queue.isEmpty()) {
            stringBuilder.append(queue.poll());

            if(!queue.isEmpty()) {
                stringBuilder.append(", ");
            }
        }

        stringBuilder.append(");");

        return "CREATE TABLE IF NOT EXISTS " + tableName + " " + stringBuilder.toString();
    }

    private static void checkIfString(Object value, StringBuilder stringBuilder) {
        if(value instanceof String) {
            stringBuilder.append("\"");
            stringBuilder.append(value);
            stringBuilder.append("\"");
        } else {
            stringBuilder.append(value);
        }
    }

}
