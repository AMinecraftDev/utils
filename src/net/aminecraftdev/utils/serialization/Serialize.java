package net.aminecraftdev.utils.serialization;

import net.aminecraftdev.utils.particles.ParticleEffect;
import net.aminecraftdev.utils.reflection.ReflectionUtils;
import org.apache.commons.codec.binary.Base64;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;

/**
 * @author Debugged
 * @version 1.0
 * @since 18-5-2017
 */
public class Serialize {

    private static Class<?> craftItemStackClazz, nmsItemStackClazz, nbtTagCompound, nbtCompressedStreamTools;
    private static Method nmsCopy, save, aOut, aIn, createStack, asBukkitCopy;

    public static String serialize(ParticleEffect particleEffect) {
        return particleEffect.getName();
    }

    public static ParticleEffect deserializeParticleEffect(String particleString) {
        return ParticleEffect.fromName(particleString);
    }

    public static String serialize(Location location) {
        return location.getWorld().getName() + ";" +
                location.getX() + ";" +
                location.getY() + ";" +
                location.getZ() + ";" +
                location.getYaw() + ";" +
                location.getPitch() + ";";
    }

    public static Location deserializeLocation(String locationString) {
        String[] split = locationString.split(";");

        try {
            World world = Bukkit.getWorld(split[0]);
            double x = Double.parseDouble(split[1]);
            double y = Double.parseDouble(split[2]);
            double z = Double.parseDouble(split[3]);
            float yaw = Float.parseFloat(split[4]);
            float pitch = Float.parseFloat(split[5]);

            return new Location(world, x, y, z, yaw, pitch);
        } catch (Exception ex) {
            return null;
        }
    }

    public static String serialize(ItemStack itemStack) {
        if(itemStack == null || itemStack.getType() == Material.AIR) return "null";

        ByteArrayOutputStream byteArrayOutputStream = null;

        try {
            if(nbtTagCompound == null) nbtTagCompound = ReflectionUtils.getNMSClass("NBTTagCompound");
            if(craftItemStackClazz == null) craftItemStackClazz = ReflectionUtils.getOBCClass("inventory.CraftItemStack");
            if(nmsItemStackClazz == null) nmsItemStackClazz = ReflectionUtils.getNMSClass("ItemStack");
            if(nbtCompressedStreamTools == null) nbtCompressedStreamTools = ReflectionUtils.getNMSClass("NBTCompressedStreamTools");

            if(nmsCopy == null) nmsCopy = craftItemStackClazz.getMethod("asNMSCopy", ItemStack.class);
            if(save == null) save = nmsItemStackClazz.getMethod("save", nbtTagCompound);
            if(aOut == null) aOut = nbtCompressedStreamTools.getMethod("a", nbtTagCompound, OutputStream.class);

            Object newNbtTagCompoundInstance = nbtTagCompound.getConstructor().newInstance();
            Object obcItemStack = nmsCopy.invoke(null, itemStack);

            save.invoke(obcItemStack, newNbtTagCompoundInstance);
            byteArrayOutputStream = new ByteArrayOutputStream();
            aOut.invoke(null, newNbtTagCompoundInstance, byteArrayOutputStream);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return Base64.encodeBase64String(byteArrayOutputStream.toByteArray());
    }

    public static ItemStack deserializeItemStacks(String data) {
        if(data == null) return null;

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(Base64.decodeBase64(data));

        try {
            if(nbtTagCompound == null) nbtTagCompound = ReflectionUtils.getNMSClass("NBTTagCompound");
            if(nmsItemStackClazz == null) nmsItemStackClazz = ReflectionUtils.getNMSClass("ItemStack");
            if(craftItemStackClazz == null) craftItemStackClazz = ReflectionUtils.getOBCClass("inventory.CraftItemStack");
            if(nbtCompressedStreamTools == null) nbtCompressedStreamTools = ReflectionUtils.getNMSClass("NBTCompressedStreamTools");

            if(aIn == null) aIn = nbtCompressedStreamTools.getMethod("a", InputStream.class);
            if(asBukkitCopy == null) asBukkitCopy = craftItemStackClazz.getMethod("asBukkitCopy", nmsItemStackClazz);

            Object compressedStream = aIn.invoke(null, byteArrayInputStream);
            Object itemStackObject;

            if(ReflectionUtils.getDoubleNMSVersion() == 1.11 || ReflectionUtils.getDoubleNMSVersion() == 1.12 || ReflectionUtils.getDoubleNMSVersion() == 1.13 || ReflectionUtils.getDoubleNMSVersion() == 1.14) {
                itemStackObject = nmsItemStackClazz.getConstructor(nbtTagCompound).newInstance(compressedStream);
            } else {
                if(createStack == null) createStack = nmsItemStackClazz.getMethod("createStack", nbtTagCompound);

                itemStackObject = createStack.invoke(null, compressedStream);
            }

            return (ItemStack) asBukkitCopy.invoke(null, itemStackObject);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }
}
