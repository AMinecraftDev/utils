package net.aminecraftdev.utils.serialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.aminecraftdev.utils.particles.ParticleEffect;
import net.aminecraftdev.utils.serialization.helpers.ItemStackAdapter;
import net.aminecraftdev.utils.serialization.helpers.LocationAdapter;
import net.aminecraftdev.utils.serialization.helpers.ParticleAdapter;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

/**
 * @author Debugged
 * @version 1.0
 * @since 18-5-2017
 */
public class BoostedGson {

    private static final Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .excludeFieldsWithoutExposeAnnotation()
            .registerTypeAdapter(Location.class, new LocationAdapter())
            .registerTypeAdapter(ItemStack.class, new ItemStackAdapter())
            .registerTypeAdapter(ParticleEffect.class, new ParticleAdapter())
            .create();

    private static final Gson gsonFlat = new GsonBuilder()
            .setPrettyPrinting()
            .excludeFieldsWithoutExposeAnnotation()
            .registerTypeAdapter(Location.class, new LocationAdapter())
            .registerTypeAdapter(ItemStack.class, new ItemStackAdapter())
            .registerTypeAdapter(ParticleEffect.class, new ParticleAdapter())
            .create();

    public static Gson getGson() {
        return gson;
    }

    public static Gson getGsonFlat() {
        return gsonFlat;
    }

}
