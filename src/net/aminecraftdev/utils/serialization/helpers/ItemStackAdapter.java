package net.aminecraftdev.utils.serialization.helpers;

import com.google.gson.*;
import net.aminecraftdev.utils.serialization.Serialize;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Type;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 20-Oct-17
 */
public class ItemStackAdapter implements JsonSerializer<ItemStack>, JsonDeserializer<ItemStack> {

    @Override
    public ItemStack deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return Serialize.deserializeItemStacks(jsonElement.getAsString());
    }

    @Override
    public JsonElement serialize(ItemStack itemStack, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(Serialize.serialize(itemStack));
    }
}
