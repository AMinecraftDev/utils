package net.aminecraftdev.utils.serialization.helpers;

import com.google.gson.*;
import net.aminecraftdev.utils.particles.ParticleEffect;
import net.aminecraftdev.utils.serialization.Serialize;

import java.lang.reflect.Type;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 03-Feb-18
 */
public class ParticleAdapter implements JsonSerializer<ParticleEffect>, JsonDeserializer<ParticleEffect> {

    @Override
    public ParticleEffect deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return Serialize.deserializeParticleEffect(jsonElement.getAsString());
    }

    @Override
    public JsonElement serialize(ParticleEffect location, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(
                Serialize.serialize(location)
        );
    }
}
