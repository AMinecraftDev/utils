package net.aminecraftdev.utils;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 06-Nov-17
 */
public interface Callback<T> {

    void call(T object);

}
