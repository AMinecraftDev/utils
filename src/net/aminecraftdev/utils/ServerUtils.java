package net.aminecraftdev.utils;

import net.aminecraftdev.utils.message.MessageUtils;
import net.aminecraftdev.utils.qplugin.QPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import java.util.Arrays;
import java.util.Optional;
import java.util.regex.Pattern;

/**
 * Created by charl on 28-Apr-17.
 */
public class ServerUtils {

    private static JavaPlugin PLUGIN;
    private static boolean DEBUG;

    public ServerUtils(QPlugin plugin) {
        DEBUG = plugin.getConfig().getBoolean("debug", false);
        PLUGIN = plugin;
    }

    public static void log(String log) {
        Bukkit.getConsoleSender().sendMessage(MessageUtils.translateString(log));
    }

    public static void logError(String log) {
        log("&c[ERROR] &7" + log);
    }

    public static void logWarn(String log) {
        log("&e[WARN] &7" + log);
    }

    public static void logDebug(String log) {
        if(!DEBUG) return;

        log("&d[DEBUG] &7" + log);
    }

    public static BukkitTask runTask(Runnable runnable) {
        return Bukkit.getScheduler().runTask(getJavaPlugin(), runnable);
    }

    public static BukkitTask runTaskLater(long time, Runnable runnable) {
        return Bukkit.getScheduler().runTaskLater(getJavaPlugin(), runnable, time);
    }
    
    public static BukkitTask runTaskTimer(long delay, long period, Runnable runnable) {
        return Bukkit.getScheduler().runTaskTimer(getJavaPlugin(), runnable, delay, period);
    }

    public static BukkitTask runTaskAsync(Runnable runnable) {
        return Bukkit.getScheduler().runTaskAsynchronously(getJavaPlugin(), runnable);
    }

    public static BukkitTask runTaskLaterAsync(long time, Runnable runnable) {
        return Bukkit.getScheduler().runTaskLaterAsynchronously(getJavaPlugin(), runnable, time);
    }
    
    public static BukkitTask runTaskTimerAsync(long delay, long period, Runnable runnable) {
        return Bukkit.getScheduler().runTaskTimerAsynchronously(getJavaPlugin(), runnable, delay, period);
    }

    public static void cancelTask(BukkitTask bukkitTask) {
        if(bukkitTask == null) return;

        bukkitTask.cancel();
    }

    public static void registerListener(Listener listener, JavaPlugin javaPlugin) {
        Bukkit.getPluginManager().registerEvents(listener, javaPlugin);
    }

    private static JavaPlugin getJavaPlugin() {
        return PLUGIN;
    }

}
