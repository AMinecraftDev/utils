package net.aminecraftdev.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 03-Jan-18
 */
public class AbstractHolder<T> {

    private Map<String, Object> dataStorage = new HashMap<>();
    private T identifier;

    public AbstractHolder(T identifier) {
        this.identifier = identifier;
    }

    public T getIdentifier() {
        return this.identifier;
    }

    public void set(String key, Object value) {
        this.dataStorage.put(key, value);
    }

    public Object get(String key) {
        return this.dataStorage.getOrDefault(key, null);
    }

    public void explode() {
        System.out.println(this.dataStorage);
    }

}
