package net.aminecraftdev.utils.dependencies;

import com.massivecraft.factions.*;
import com.massivecraft.factions.struct.Relation;
import net.aminecraftdev.utils.IHelper;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 13-Aug-17
 */
public class FactionHelper implements IHelper {

    private static FactionHelper instance;
    private Boolean bool;

    public FactionHelper() {
        instance = this;
    }

    @Override
    public boolean isConnected() {
        return bool != null? bool : (bool = Bukkit.getPluginManager().getPlugin("Factions") != null);
    }

    public static boolean isFriendly(Player playerA, Player playerB) {
        return isFriendly(playerA, playerB.getLocation());
    }

    public static boolean isFriendly(Player playerA, Location locationB) {
        Faction factionA = FPlayers.getInstance().getByPlayer(playerA).getFaction();
        FLocation fLocationB = new FLocation(locationB);
        Faction factionB = Board.getInstance().getFactionAt(fLocationB);

        return (factionA == factionB) || (factionA.getRelationTo(factionB).isAtLeast(Relation.ALLY));
    }

    public static boolean canPvP(Player playerA, Player playerB) {
        Faction factionA = FPlayers.getInstance().getByPlayer(playerA).getFaction();
        Faction factionB = FPlayers.getInstance().getByPlayer(playerB).getFaction();

        return !(factionA == factionB) || (factionB.isWilderness()) || !(factionA.getRelationTo(factionB).isAtLeast(Relation.TRUCE));
    }

    public static boolean canFactionMine(Player playerA, Location locationB) {
        Faction factionA = FPlayers.getInstance().getByPlayer(playerA).getFaction();
        FLocation fLocationB = new FLocation(locationB);
        Faction factionB = Board.getInstance().getFactionAt(fLocationB);

        return (factionA == factionB) || (factionB.isWilderness()) || (factionA.getRelationTo(factionB).isAtLeast(Relation.ALLY));
    }

    public static boolean canPvP(Location locationB) {
        FLocation fLocationB = new FLocation(locationB);
        Faction factionB = Board.getInstance().getFactionAt(fLocationB);

        return !factionB.isSafeZone();
    }

    public static FactionHelper get() {
        return instance;
    }
}
