package net.aminecraftdev.utils.dependencies;

import de.myzelyam.api.vanish.VanishAPI;
import net.aminecraftdev.utils.IHelper;
import org.bukkit.Bukkit;

import java.util.UUID;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 13-Dec-17
 */
public class PremiumVanishHelper implements IHelper {

    private static PremiumVanishHelper instnace;

    private Boolean bool;

    public PremiumVanishHelper() {
        instnace = this;
    }

    @Override
    public boolean isConnected() {
        return bool != null? bool : (bool = Bukkit.getPluginManager().getPlugin("PremiumVanish") != null);
    }

    public static boolean isVanished(UUID uuid) {
        return get().isConnected() && VanishAPI.isInvisible(Bukkit.getPlayer(uuid));
    }

    public static PremiumVanishHelper get() {
        return instnace;
    }
}
