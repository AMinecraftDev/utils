package net.aminecraftdev.utils.dependencies;

import com.wasteofplastic.askyblock.ASkyBlock;
import com.wasteofplastic.askyblock.Island;
import com.wasteofplastic.askyblock.Settings;
import net.aminecraftdev.utils.IHelper;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 17-Dec-17
 */
public class ASkyblockHelper implements IHelper {

    private static ASkyblockHelper instance;
    private static ASkyBlock aSkyBlock;
    private Boolean isConnected;

    public ASkyblockHelper() {
        instance = this;
        aSkyBlock = (ASkyBlock) Bukkit.getPluginManager().getPlugin("ASkyBlock");
    }

    @Override
    public boolean isConnected() {
        return this.isConnected == null? (this.isConnected = aSkyBlock == null) : this.isConnected;
    }

    public boolean canBreakBlock(Player player, Location location) {
        return actionAllowed(player, location, Island.SettingsFlag.BREAK_BLOCKS);
    }

    private static boolean actionAllowed(Player player, Location location, Island.SettingsFlag settingsFlag) {
        if(player == null) return actionAllowed(location, settingsFlag);
        if(player.isOp() || player.hasPermission("askyblock.mod.bypassprotect")) return true;

        Island island = aSkyBlock.getGrid().getProtectedIslandAt(location);

        if(island != null && (island.getIgsFlag(settingsFlag) || island.getMembers().contains(player.getUniqueId()))) return true;
        if(island == null && Settings.defaultWorldSettings.get(settingsFlag)) return true;
        return false;
    }

    private static boolean actionAllowed(Location location, Island.SettingsFlag settingsFlag) {
        Island island = aSkyBlock.getGrid().getProtectedIslandAt(location);

        if(island != null && island.getIgsFlag(settingsFlag)) return true;
        if(island == null && Settings.defaultWorldSettings.get(settingsFlag)) return true;
        return false;
    }

    public static ASkyblockHelper get() {
        return instance;
    }
}
