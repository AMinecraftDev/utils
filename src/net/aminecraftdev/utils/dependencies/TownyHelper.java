package net.aminecraftdev.utils.dependencies;

import com.palmergames.bukkit.towny.object.TownyUniverse;
import net.aminecraftdev.utils.IHelper;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 10-Nov-17
 */
public class TownyHelper implements IHelper {

    private static TownyHelper instance;
    private Boolean bool;

    public TownyHelper() {
        instance = this;
    }

    @Override
    public boolean isConnected() {
        return bool != null? bool : (bool = Bukkit.getPluginManager().getPlugin("Towny") != null);
    }

    public static String getTownStanding(Player player) {
        return TownyUniverse.getTownName(player.getLocation());
    }

    public static TownyHelper get() {
        return instance;
    }

}
