package net.aminecraftdev.utils.dependencies;

import com.earth2me.essentials.Essentials;
import net.aminecraftdev.utils.IHelper;
import org.bukkit.Bukkit;

import java.util.UUID;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 11-Dec-17
 */
public class EssentialsHelper implements IHelper {

    private static EssentialsHelper instance;

    private Essentials essentials;
    private Boolean bool;

    public EssentialsHelper() {
        instance = this;
        this.essentials = ((Essentials) Bukkit.getPluginManager().getPlugin("Essentials"));
    }

    @Override
    public boolean isConnected() {
        return bool != null? bool : (bool = essentials != null);
    }

    public boolean isVanished(UUID uuid) {
        return this.essentials.getUser(uuid).isVanished();
    }

    public static EssentialsHelper get() {
        return instance;
    }

}
