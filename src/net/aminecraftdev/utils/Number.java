package net.aminecraftdev.utils;

import java.math.BigInteger;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 03-Jan-18
 */
public enum Number {

    SINGLE(1),
    TENS(10),
    HUNDRED(100),
    THOUSAND(1000),
    TENS_THOUSAND(10000),
    HUNDREDS_THOUSAND(100000),
    MILLION(1000000),
    TENS_MILLION(10000000),
    HUNDREDS_MILLION(100000000),
    BILLION(1000000000);

    private int multiplied;

    Number(int multiplied) {
        this.multiplied = multiplied;
    }

    public BigInteger get(int num) {
        return BigInteger.valueOf(num * this.multiplied);
    }

}
