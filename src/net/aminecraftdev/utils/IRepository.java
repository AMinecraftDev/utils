package net.aminecraftdev.utils;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 08-Dec-17
 */
public interface IRepository<TType, TUnique> {

    void Create(TType entity); //Add an entity (ONLY if it's new. Throw an error if it already exists. This is important)

    TUnique Read(TType entity); //Get an Entity by Id

    void Update(TType entity, TUnique id); //Update that Entity

    void Delete(TType entity); //Delete by entity (get the UniqueId from it)

}
