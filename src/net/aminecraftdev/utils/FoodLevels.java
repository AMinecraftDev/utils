package net.aminecraftdev.utils;

import org.bukkit.Material;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 14-Aug-17
 */
public enum FoodLevels {

    Apple(Material.APPLE, 4, 2.4),
    MushroomStew(Material.MUSHROOM_SOUP, 6, 7.2),
    Bread(Material.BREAD, 5, 6.0),
    RawPorkchop(Material.PORK, 3, 0.6),
    GoldenApple(Material.GOLDEN_APPLE, 4, 9.6),
    RawFish(Material.RAW_FISH, 2, 0.4),
    RawSalmon(Material.RAW_FISH, 1, 2, 0.2),
    RawClownfish(Material.RAW_FISH, 2, 1, 0.2),
    Pufferfish(Material.RAW_FISH, 3, 1, 0.2, new PotionEffect(PotionEffectType.HUNGER, 15*20, 2), new PotionEffect(PotionEffectType.POISON, 60*20, 3), new PotionEffect(PotionEffectType.CONFUSION, 15*20, 1)),
    CookedFish(Material.COOKED_FISH, 5, 6),
    CookedSalmon(Material.COOKED_FISH, 6, 9.6),
    Cookie(Material.COOKIE, 2, 0.5),
    Melon(Material.MELON, 2, 1.2),
    RawBeef(Material.RAW_BEEF, 3, 1.8),
    CookedBeef(Material.COOKED_BEEF, 8, 12.8),
    RawChicken(Material.RAW_CHICKEN, 0, 2, 1.2, new PotionEffect(PotionEffectType.HUNGER, 30*20, 0)),
    CookedChicken(Material.COOKED_CHICKEN, 6, 7.2),
    RottenFlesh(Material.ROTTEN_FLESH, 0, 4, 0.8, new PotionEffect(PotionEffectType.HUNGER, 30*20, 0)),
    SpiderEye(Material.SPIDER_EYE, 0, 2, 3.2, new PotionEffect(PotionEffectType.POISON, 4*20, 0)),
    Carrot(Material.CARROT_ITEM, 3, 3.6),
    Potato(Material.POTATO_ITEM, 1, 0.6),
    CookedPotato(Material.BAKED_POTATO, 5, 6),
    PoisonousPotato(Material.POISONOUS_POTATO, 0, 2, 1.2, new PotionEffect(PotionEffectType.POISON, 4*20, 0)),
    PumpkinPie(Material.PUMPKIN_PIE, 8, 4.8),
    RawRabbit(Material.RABBIT, 3, 1.8),
    CookedRabbit(Material.COOKED_RABBIT, 5, 6),
    RabbitStew(Material.RABBIT_STEW, 10, 12),
    RawMutton(Material.MUTTON, 2, 1.2),
    CookedMutton(Material.COOKED_MUTTON, 6, 9.6),
    Beetroot(Material.BEETROOT, 1, 1.2),
    BeetrootSoup(Material.BEETROOT_SOUP, 6, 7.2);

    private Material material;
    private int hunger;
    private int data;
    private double saturation;
    private List<PotionEffect> potionEffects = new ArrayList<>();

    FoodLevels(Material material, int hunger, double saturation) {
        this(material, 0, hunger, saturation);
    }

    FoodLevels(Material material, int data, int hunger, double saturation) {
        this.material = material;
        this.data = data;
        this.hunger = hunger;
        this.saturation = saturation;
    }

    FoodLevels(Material material, int data, int hunger, double saturation, PotionEffect... potionEffects) {
        this.material = material;
        this.data = data;
        this.hunger = hunger;
        this.saturation = saturation;
        this.potionEffects.addAll(Arrays.asList(potionEffects));
    }

    public Material getMaterial() {
        return material;
    }

    public int getData() {
        return data;
    }

    public int getHunger() {
        return hunger;
    }

    public double getSaturation() {
        return saturation;
    }

    public List<PotionEffect> getPotionEffects() {
        return potionEffects;
    }

    public static FoodLevels getFood(Material material, int data) {
        for(FoodLevels foodLevels : values()) {
            Material flMaterial = foodLevels.getMaterial();
            int flData = foodLevels.getData();

            if(material == flMaterial && data == flData) return foodLevels;
        }

        return null;
    }
}
