package net.aminecraftdev.utils.message;

import net.aminecraftdev.utils.RandomUtils;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 06-Jul-17
 */
public class Captcha {

    private static char DATA[] = {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
            'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
            'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
            'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
            'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6',
            '7', '8', '9'
    };

    public static String generateCaptcha() {
        return generateCaptcha(9);
    }

    public static String generateCaptcha(int length) {
        char index[] = new char[length];

        for(int i = 0; i < index.length; i++) {
            int random = RandomUtils.getRandomInt(DATA.length);

            index[i] = DATA[random];
        }

        return new String(index);
    }

}
