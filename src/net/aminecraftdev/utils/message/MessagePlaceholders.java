package net.aminecraftdev.utils.message;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 08-Feb-18
 */
public enum MessagePlaceholders {

    Colors_Primary("&b", "{cp}"),
    Colors_Secondary("&e", "{cs}"),
    Name_Short("Boosted", "{ns}"),
    Name_Full("BoostedNetwork", "{nf}"),
    Links_IP("mc.boostednetwork.org", "{li}"),
    Links_Store("store.boostednetwork.org", "{ls}");

    private static FileConfiguration LANG;

    private final String defaultMessage, path, placeholder;

    MessagePlaceholders(String defaultMessage, String placeholder) {
        this.path = name().replace("_", ".");
        this.defaultMessage = defaultMessage;
        this.placeholder = placeholder;
    }

    public String getPlaceholder() {
        return this.placeholder;
    }

    @Override
    public String toString() {
        return ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, this.defaultMessage));
    }

    public static void setFile(FileConfiguration configuration) {
        LANG = configuration;
    }

    public static String replaceMessage(String message) {
        for(MessagePlaceholders messagePlaceholders : values()) {
            if(message.contains(messagePlaceholders.getPlaceholder())) {
                message = message.replace(messagePlaceholders.getPlaceholder(), messagePlaceholders.toString());
            }
        }

        return message;
    }

}
