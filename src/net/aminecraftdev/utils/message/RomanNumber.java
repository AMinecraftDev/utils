package net.aminecraftdev.utils.message;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by charl on 07-May-17.
 */
public class RomanNumber {

    private static TreeMap<Integer, String> map = new TreeMap<>();
    private static TreeMap<String, Integer> reverseMap = new TreeMap<>();

    static {
        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(1, "I");

        for(Map.Entry<Integer, String> entry : map.entrySet()) {
            reverseMap.put(entry.getValue(), entry.getKey());
        }
    }

    public static String toRoman(int number) {
        int l =  map.floorKey(number);
        if ( number == l ) {
            return map.get(number);
        }
        return map.get(l) + toRoman(number-l);
    }

    public static int getFromRoman(String text) {
        int totalValue = 0, prevValue = 0;

        for(char cha : text.toCharArray()) {
            String str = String.valueOf(cha);
            if(!reverseMap.containsKey(str)) return 0;

            int charValue = reverseMap.get(str);

            totalValue += charValue;

            if(prevValue != 0 && prevValue < charValue) {
                if (prevValue == 1 && (charValue == 5 || charValue == 10)
                        || prevValue == 10 && (charValue == 50 || charValue == 100)
                        || prevValue == 100 && (charValue == 500 || charValue == 1000)) {
                    totalValue -= 2 * prevValue;
                } else {
                    return 0;
                }
            }

            prevValue = charValue;
        }

        return totalValue;
    }

}
