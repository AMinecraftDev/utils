package net.aminecraftdev.utils.qplugin;

import net.aminecraftdev.utils.ServerUtils;
import net.aminecraftdev.utils.UtilLoader;
import net.aminecraftdev.utils.command.builder.CommandService;
import net.aminecraftdev.utils.core.PlayerData;
import net.aminecraftdev.utils.file.YmlFile;
import net.aminecraftdev.utils.builders.files.YmlBuilder;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 10-Nov-17
 */
public abstract class SimpleQPlugin extends JavaPlugin {

    private Set<CommandService<? extends CommandSender>> commandServices = new HashSet<>();
    private Set<YmlFile> ymlFiles = new HashSet<>();
    private File pluginData = new File(getDataFolder(), "plugin-data");
    private Set<Listener> listeners = new HashSet<>();

    private YmlFile config, lang;

    @Override
    public void onEnable() {
        if(UtilLoader.get() == null || !UtilLoader.get().hasBeenLoaded1()) {
            if(!new UtilLoader().loadHalf(this)) return;
        }

        if(!this.pluginData.exists()) {
            this.pluginData.mkdir();
        }

        loadFiles();
        loadSettings();
        loadLang();

        enableStage1();

        reloadFiles();
        loadCommands();
        loadListeners();

        this.listeners.forEach(listener -> ServerUtils.registerListener(listener, this));
        Bukkit.getOnlinePlayers().forEach(player -> PlayerData.createPlayerData(player.getUniqueId()));
    }

    @Override
    public void onDisable() {
        disableStage1();
    }

    protected abstract void enableStage1();

    protected abstract void disableStage1();

    protected abstract void loadMessages();

    protected abstract void loadFiles();

    protected abstract void loadCommands();

    protected abstract void loadListeners();

    protected abstract void loadSettings();

    public void saveFiles() {
        this.ymlFiles.forEach(file -> file.saveFile(file.getConfig()));
    }

    public void reloadFiles() {
        this.ymlFiles.forEach(YmlFile::loadFile);
    }

    public YmlFile getFile(String path) {
        return this.ymlFiles.stream().filter(file -> file.getKey().equals(path)).findFirst().orElse(null);
    }

    public void setConfig(YmlFile ymlFile) {
        this.config = ymlFile;
        addFile(ymlFile);
    }

    public void addCommand(CommandService<? extends CommandSender> commandService) {
        this.commandServices.add(commandService);
    }

    public void addListener(Listener listener) {
        this.listeners.add(listener);
    }

    public void addFile(YmlFile file) {
        this.ymlFiles.add(file);
    }

    public void removeFile(YmlFile file) {
        this.ymlFiles.remove(file);
    }

    public File getPluginData() {
        return this.pluginData;
    }

    public Set<Listener> getListeners() {
        return this.listeners;
    }

    public Set<CommandService<? extends CommandSender>> getCommandServices() {
        return this.commandServices;
    }

    @Override
    public FileConfiguration getConfig() {
        return this.config.getConfig();
    }

    public FileConfiguration getLang() {
        return this.lang.getConfig();
    }

    public void log(String message) {
        ServerUtils.log(message);
    }

    private void loadLang() {
        File langFile = new File(getDataFolder(), "lang.yml");

        this.lang = new YmlBuilder(langFile)
                .saveResource(false)
                .setMoveOld(false)
                .build();

        addFile(this.lang);
        loadMessages();
    }

}
