package net.aminecraftdev.utils.qplugin;

import net.aminecraftdev.utils.UtilLoader;
import net.aminecraftdev.utils.database.hikari.HikariCP;
import net.aminecraftdev.utils.file.YmlFile;
import net.aminecraftdev.utils.module.ModuleManager;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 20-Oct-17
 */
public abstract class QPlugin extends SimpleQPlugin {

    private YmlFile inventories;
    private HikariCP hikariCP;

    @Override
    public void enableStage1() {
        if(UtilLoader.get() == null || !UtilLoader.get().hasBeenLoaded2()) {
            UtilLoader.get().loadFull(this);
        }

        loadSQL();
        enableStage2();
    }

    @Override
    public void disableStage1() {
        disableStage2();
        ModuleManager.get().disable();
    }

    protected abstract void enableStage2();

    protected abstract void disableStage2();

    protected abstract void loadSQL();

    public FileConfiguration getInventories() {
        return this.inventories.getConfig();
    }

    public void setInventories(YmlFile ymlFile) {
        this.inventories = ymlFile;
        addFile(ymlFile);
    }

    public HikariCP getHikariCP() {
        return this.hikariCP;
    }

    public void setHikariCP(HikariCP hikariCP) {
        this.hikariCP = hikariCP;
    }


}
