package net.aminecraftdev.utils.voucher.base;

import org.bukkit.event.player.PlayerInteractEvent;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 25-May-17
 */
public interface VoucherClickAction {

    /**
     * Called whenever the item that this
     * ClickAction is associated with is triggered
     *
     * @param event PlayerInteractEvent
     */
    boolean onInteract(PlayerInteractEvent event);
}
