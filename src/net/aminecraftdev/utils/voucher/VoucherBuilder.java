package net.aminecraftdev.utils.voucher;

import net.aminecraftdev.utils.SoundUtils;
import net.aminecraftdev.utils.itemstack.ItemStackUtils;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 25-May-17
 */
public class VoucherBuilder {

    private ConfigurationSection configurationSection, soundSection;
    private Map<String, String> replaceMap;

    private ItemStack itemStack;
    private Sound sound;
    private List<String> compoundList = null;

    public VoucherBuilder(ConfigurationSection configurationSection) {
        this(configurationSection, null);
    }

    public VoucherBuilder(ConfigurationSection configurationSection, Map<String, String> replaceMap) {
        this(configurationSection, replaceMap, null);
    }

    public VoucherBuilder(ConfigurationSection configurationSection, Map<String, String> replaceMap, ConfigurationSection soundSection) {
        this(configurationSection, replaceMap, soundSection, null);
    }

    public VoucherBuilder(ConfigurationSection configurationSection, Map<String, String> replaceMap, ConfigurationSection soundSection, List<String> compoundData) {
        this.configurationSection = configurationSection;
        this.soundSection = soundSection;
        this.replaceMap = replaceMap;
        this.compoundList = compoundData;

        build();
    }

    public Voucher getVoucher() {
        return new Voucher(this.itemStack, this.compoundList, this.sound);
    }

    public ItemStack build() {
        if(soundSection != null) this.sound = SoundUtils.getSound(soundSection);

        return (this.itemStack = ItemStackUtils.createItemStack(configurationSection, 1, replaceMap));
    }
}