package net.aminecraftdev.utils.voucher;

import net.aminecraftdev.utils.PluginUtils;
import net.aminecraftdev.utils.factory.NbtFactory;
import net.aminecraftdev.utils.itemstack.ItemStackUtils;
import net.aminecraftdev.utils.voucher.base.VoucherClickAction;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 25-May-17
 */
public class Voucher {

    private static List<Voucher> vouchers = new ArrayList<>();
    private static boolean registered = false;
    private static JavaPlugin javaPlugin;

    private List<VoucherClickAction> actions = new ArrayList<>();
    private boolean takeItemWhenDone = false;
    private boolean checkIfSimilar = true;
    private ItemStack voucher;
    private Sound redeemSound;
    private List<String> compoundPaths;

    public Voucher(ItemStack itemStack) {
        this(itemStack, null, null);
    }

    public Voucher(ItemStack itemStack, List<String> compoundPaths) {
        this(itemStack, compoundPaths, null);
    }

    public Voucher(ItemStack itemStack, Sound redeemSound) {
        this(itemStack, null, redeemSound);
    }

    public Voucher(ItemStack itemStack, List<String> compoundPaths, Sound redeemSound) {
        this.voucher = itemStack;
        this.redeemSound = redeemSound;
        this.compoundPaths = compoundPaths;

        if(!registered) {
            Bukkit.getPluginManager().registerEvents(getListeners(), javaPlugin);
            registered = true;
        }

        vouchers.add(this);
    }

    /**
     * Returns the listeners that are
     * needed for all Panel handlers to work properly
     *
     * @return Listener
     */
    private Listener getListeners() {
        return new Listener() {

            @EventHandler
            public void onClick(PlayerInteractEvent event) {
                Player player = event.getPlayer();
                ItemStack itemStack = PluginUtils.getItemInHand(player);
                ItemStack clone = itemStack.clone();

                if(!event.getAction().toString().startsWith("RIGHT_CLICK")) return;
                if(Voucher.vouchers.isEmpty()) return;
                if(itemStack == null || itemStack.getType() == Material.AIR) return;

                clone.setAmount(1);

                for(Voucher voucher : new ArrayList<>(Voucher.vouchers)) {
                    if(voucher == null || voucher.getVoucher() == null) continue;
                    if(voucher.isCheckIfSimilar()) if(!clone.isSimilar(voucher.getVoucher())) continue;

                    if(voucher.getCompoundPaths() != null) {
                        NbtFactory.NbtCompound itemStackCompound = NbtFactory.fromItemTag(NbtFactory.getCraftItemStack(itemStack));
                        boolean doesntContainAllPaths = false;

                        if(itemStackCompound == null || itemStackCompound.isEmpty()) continue;

                        for(String s : voucher.getCompoundPaths()) {
                            if(!itemStackCompound.containsKey(s)) {
                                doesntContainAllPaths = true;
                                break;
                            }
                        }

                        if(doesntContainAllPaths) continue;
                    }

                    if(!voucher.executeAction(event)) return;
                    if(voucher.getRedeemSound() != null) player.playSound(player.getLocation(), voucher.getRedeemSound(), 3F, 1F);
                    if(voucher.isTakeItemWhenDone()) player.getInventory().removeItem(clone);
                    break;
                }
            }
        };
    }

    /**
     * Triggers the VoucherClickAction interfaces
     * for the specific voucher
     *
     * @return
     */
    public boolean executeAction(PlayerInteractEvent event) {
        for(VoucherClickAction action : actions) {
            if(!action.onInteract(event)) return false;
        }

        return true;
    }


    public ItemStack getVoucher() {
        return voucher;
    }

    public Sound getRedeemSound() {
        return redeemSound;
    }

    public List<String> getCompoundPaths() {
        return compoundPaths;
    }

    public NbtFactory.NbtCompound getCompound(ItemStack itemStack) {
        return NbtFactory.fromItemTag(NbtFactory.getCraftItemStack(itemStack));
    }

    public boolean isCheckIfSimilar() {
        return checkIfSimilar;
    }

    public boolean isTakeItemWhenDone() {
        return takeItemWhenDone;
    }

    public void givePlayerVoucher(Player player, int amount, Map<String, String> replaceables, Map<String, Object> compound) {
        ItemStack itemStack = ItemStackUtils.createItemStack(getVoucher().clone(), replaceables, compound);

        itemStack.setAmount(amount);

        player.getInventory().addItem(itemStack);
    }

    public Voucher setCheckIfSimilar(boolean bool) {
        this.checkIfSimilar = bool;
        return this;
    }

    public Voucher setTakeItemWhenDone(boolean bool) {
        this.takeItemWhenDone = bool;
        return this;
    }

    public Voucher setOnRedeem(VoucherClickAction voucherClickAction) {
        actions.add(voucherClickAction);
        return this;
    }

    public static void setJavaPlugin(JavaPlugin newJavaPlugin) {
        Voucher.javaPlugin = newJavaPlugin;
    }
}
