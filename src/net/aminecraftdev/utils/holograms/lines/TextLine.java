package net.aminecraftdev.utils.holograms.lines;

import net.aminecraftdev.utils.holograms.HologramLine;
import net.aminecraftdev.utils.message.MessageUtils;
import org.bukkit.entity.ArmorStand;

/**
 * @author Debugged
 * @version 1.0
 * @since 20-5-2017
 */
public class TextLine extends HologramLine {

    private String text;

    public TextLine(String text) {
        super(0.28);
        this.text = text;
    }

    @Override
    public void display(ArmorStand armorStand) {
        armorStand.setCustomNameVisible(true);
        armorStand.setCustomName(MessageUtils.translateString(text));

        this.armorStand = armorStand;
    }

    @Override
    public void remove() {
        if(armorStand == null) return;

        armorStand.remove();
    }

    public String getText() {
        return text;
    }

}