package net.aminecraftdev.utils.holograms.lines;

import net.aminecraftdev.utils.holograms.HologramLine;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

/**
 * @author Debugged
 * @version 1.0
 * @since 21-5-2017
 */
public class ItemLine extends HologramLine {

    private ItemStack itemStack;

    public ItemLine(ItemStack itemStack) {
        super(0.50);
        this.itemStack = itemStack;
    }

    @Override
    public void display(ArmorStand armorStand) {
        Item item = armorStand.getWorld().dropItemNaturally(armorStand.getEyeLocation(), itemStack);
        item.setPickupDelay(Integer.MAX_VALUE);
        item.setVelocity(new Vector(0, 0, 0));
        armorStand.addPassenger(item);
        armorStand.setCustomNameVisible(false);

        this.armorStand = armorStand;
    }

    @Override
    public void remove() {
        ArmorStand holder = this.armorStand;
        if(holder == null) return;

        holder.getPassengers().forEach(Entity::remove);
        holder.remove();
    }

}