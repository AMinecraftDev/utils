package net.aminecraftdev.utils.holograms;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Debugged
 * @version 1.0
 * @since 20-5-2017
 */
public class Hologram {

    private static final List<Hologram> holograms = new ArrayList<>();

    private Location location;
    private List<HologramLine> lines;

    public Hologram(Location location, List<HologramLine> lines) {
        this.location = location;
        this.lines = lines;
    }

    public Hologram(Location location) {
        this(location, new ArrayList<>());
    }

    public void setLine(int index, HologramLine line) {
        if(lines.size() <= index || index < 0) {
            throw new IllegalArgumentException(String.format("Invalid hologram line index supplied. (%s out of max %s)", index, lines.size()));
        }

        HologramLine current = lines.get(index);
        ArmorStand holder = current.getHolder();
        holder.setCustomNameVisible(false);
        holder.getPassengers().forEach(Entity::remove);
        line.display(holder);

        lines.set(index, line);
        for (int i = index; i < lines.size(); i++) {
            reposition(lines.get(i));
        }
    }

    private void reposition(HologramLine line) {
        int index = lines.indexOf(line);
        double height = getHeight(index) + (lines.size() != 0 ? lines.get(index-1).getHeight() > line.getHeight() ? lines.get(index-1).getHeight() : line.getHeight() : line.getHeight());
        line.getHolder().getLocation().setY(location.clone().getY() - height);
    }

    public void addLine(HologramLine line) {
        double toRemove = lines.size() > 0 ?
                lines.get(lines.size()-1).getHeight() > line.getHeight() ? lines.get(lines.size()-1).getHeight() : line.getHeight()
                : line.getHeight();
        Location hologramLocation = this.location.clone().subtract(0, getTotalHeight() + toRemove, 0);
        ArmorStand holder = hologramLocation.getWorld().spawn(hologramLocation, ArmorStand.class);
        holder.setVisible(false);
        holder.setSmall(true);
        holder.setArms(false);
        holder.setBasePlate(false);
        holder.setMarker(true);
        holder.setGravity(false);

        line.display(holder);

        lines.add(line);
    }

    public void destroy() {
        lines.forEach(HologramLine::remove);
        lines.clear();
        lines = null;
        location = null;
    }

    private double getHeight(int startIndex) {
        return lines.stream()
                .skip(startIndex+1)
                .mapToDouble(HologramLine::getHeight)
                .sum();
    }

    private double getTotalHeight() {
        return lines
                .stream()
                .mapToDouble(HologramLine::getHeight)
                .sum();
    }

    public static void removeAll() {
        holograms.forEach(Hologram::destroy);
        holograms.clear();
    }

}