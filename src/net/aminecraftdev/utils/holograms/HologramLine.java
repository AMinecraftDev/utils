package net.aminecraftdev.utils.holograms;

import org.bukkit.entity.ArmorStand;

/**
 * @author Debugged
 * @version 1.0
 * @since 20-5-2017
 */
public abstract class HologramLine {

    private double height;
    protected ArmorStand armorStand = null;

    public HologramLine(double height) {
        this.height = height;
    }

    public abstract void display(ArmorStand armorStand);

    public abstract void remove();

    public ArmorStand getHolder() {
        return armorStand;
    }

    public double getHeight() {
        return height;
    }

}