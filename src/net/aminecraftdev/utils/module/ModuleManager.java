package net.aminecraftdev.utils.module;

import net.aminecraftdev.utils.IModule;
import net.aminecraftdev.utils.Reloadable;
import net.aminecraftdev.utils.ServerUtils;
import net.aminecraftdev.utils.qplugin.QPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 05-Feb-18
 */
public class ModuleManager implements Reloadable {

    private static ModuleManager INSTANCE;
    private static QPlugin PLUGIN;

    private List<IModule> moduleList = new ArrayList<>();

    public ModuleManager(QPlugin plugin) {
        INSTANCE = this;
        PLUGIN = plugin;
    }

    @Override
    public void reload() {
        this.moduleList.forEach(module -> {
            ServerUtils.log("&aModule " + module.getModuleName() + " has been enabled.");
            module.enable();
        });
    }

    public void disable() {
        this.moduleList.forEach(module -> {
            ServerUtils.log("&aModule " + module.getModuleName() + " has been disabled.");
            module.disable();
        });
    }

    public void add(IModule module) {
        this.moduleList.add(module);
    }

    public void remove(IModule module) {
        this.moduleList.remove(module);
    }

    public static ModuleManager get() {
        return INSTANCE;
    }

    public static QPlugin getPlugin() {
        return PLUGIN;
    }
}
