package net.aminecraftdev.utils;

import net.aminecraftdev.utils.reflection.ReflectionUtils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by charl on 28-Apr-17.
 */
public class PotionUtils extends ReflectionUtils {

    public static final PotionEffect getPotionEffect(ConfigurationSection configurationSection) {
        int level = configurationSection.getInt("level");
        int duration = configurationSection.getInt("duration");
        String type = configurationSection.getString("type");

        if(PotionEffectType.getByName(type) == null) {
            return null;
        }

        PotionEffectType potionEffectType = PotionEffectType.getByName(type);

        if(duration == -1) {
            duration = 100000;
        } else {
            duration *= 20;
        }

        level -= 1;

        return new PotionEffect(potionEffectType, duration, level);
    }

    public static PotionEffectType getGlowing() {
        if(getAPIVersion().startsWith("v1_8") || getAPIVersion().startsWith("v1_7")) {
            return PotionEffectType.getByName("night_vision");
        } else {
            return PotionEffectType.getByName("glowing");
        }
    }
}
