package net.aminecraftdev.utils;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Jan-18
 */
public interface IModule {

    void enable();

    void disable();

    String getModuleName();

}
