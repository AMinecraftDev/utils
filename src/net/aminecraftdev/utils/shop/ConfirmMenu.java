package net.aminecraftdev.utils.shop;

import net.aminecraftdev.utils.NumberUtils;
import net.aminecraftdev.utils.Reloadable;
import net.aminecraftdev.utils.inventory.Panel;
import net.aminecraftdev.utils.message.MessageUtils;
import net.aminecraftdev.utils.shop.base.IShopCancel;
import net.aminecraftdev.utils.shop.base.IShopConfirm;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 03-Jan-18
 */
public class ConfirmMenu extends Panel implements Reloadable {

    private IShopConfirm shopConfirm = null;
    private IShopCancel shopCancel = null;
    private double balance, cost;

    public ConfirmMenu(String title, double balance, double cost) {
        super(title, 9);

        this.balance = balance;
        this.cost = cost;
    }

    public ConfirmMenu setShopConfirm(IShopConfirm shopConfirm) {
        this.shopConfirm = shopConfirm;
        return this;
    }

    public ConfirmMenu setShopCancel(IShopCancel shopCancel) {
        this.shopCancel = shopCancel;
        return this;
    }


    @Override
    public void reload() {
        ItemStack pane = new ItemStack(Material.THIN_GLASS);
        ItemStack confirm = new ItemStack(Material.EMERALD);
        ItemStack cancel = new ItemStack(Material.REDSTONE);
        ItemStack information = new ItemStack(Material.BOOK);
        ItemMeta itemMeta = confirm.getItemMeta();

        itemMeta.setDisplayName(MessageUtils.translateString("&aConfirm Purchase"));
        itemMeta.setLore(Arrays.asList(MessageUtils.translateString("&7Your purchase was greater then $5 million,"), MessageUtils.translateString("&7so click here to confirm it.")));
        confirm.setItemMeta(itemMeta);

        itemMeta = cancel.getItemMeta();
        itemMeta.setDisplayName(MessageUtils.translateString("&cCancel Purchase"));
        itemMeta.setLore(Arrays.asList(MessageUtils.translateString("&7Your purchase was greater then $5 million,"), MessageUtils.translateString("&7so click here to cancel it.")));
        cancel.setItemMeta(itemMeta);

        itemMeta = information.getItemMeta();
        itemMeta.setDisplayName(MessageUtils.translateString("&bInformation"));
        itemMeta.setLore(Arrays.asList(MessageUtils.translateString("&d&lBalance: &f$" + NumberUtils.formatDouble(this.balance)), MessageUtils.translateString("&d&lCost: &f$" + NumberUtils.formatDouble(this.cost))));
        information.setItemMeta(itemMeta);

        setItem(0, pane);
        setItem(1, pane);
        setItem(2, pane);
        setItem(3, cancel, this.shopCancel::setOnCancel);
        setItem(4, information);
        setItem(5, confirm, this.shopConfirm::setOnConfirm);
        setItem(6, pane);
        setItem(7, pane);
        setItem(8, pane);

        setOnClick(event -> event.getWhoClicked().closeInventory());
    }
}
