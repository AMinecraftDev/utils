package net.aminecraftdev.utils.shop.base;

import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 03-Jan-18
 */
@FunctionalInterface
public interface IShopConfirm {

    void setOnConfirm(InventoryClickEvent event);

}
