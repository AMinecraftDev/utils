package net.aminecraftdev.utils;

/**
 * Created by charl on 07-May-17.
 */
public class TimerUtils {

    public static long getCurrentMillis() {
        return System.currentTimeMillis();
    }

    public static long getTimeBetween(long first, long second) {
        return (first - second);
    }

    public static long getRemainingSec(long millis) {
        long seconds = millis / 1000L;

        while(seconds > 60L) {
            seconds -= 60L;
        }

        return seconds;
    }

    public static long getRemainingMin(long millis) {
        long seconds = millis / 1000L;
        long minutes = 0L;

        while(seconds > 60L) {
            seconds -= 60L;
            minutes += 1L;
        }

        while(minutes > 60L) {
            minutes -= 60L;
        }

        return minutes;
    }

    public static long getRemainingHour(long millis) {
        long seconds = millis / 1000L;
        long minutes = 0L;
        long hours = 0L;

        while(seconds > 60L) {
            seconds -= 60L;
            minutes += 1L;
        }

        while(minutes > 60L) {
            minutes -= 60L;
            hours += 1L;
        }

        return hours;
    }
}
