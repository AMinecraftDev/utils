package net.aminecraftdev.utils;

import java.io.File;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 05-Feb-18
 */
public interface IFile<T, F> {

    void saveFile(T t);

    T loadFile();

    F getConfig();

    void delete();

    void rename(File file);

    String getKey();

}
