package net.aminecraftdev.utils;

import net.aminecraftdev.utils.qplugin.QPlugin;
import org.bukkit.event.Listener;

import java.io.File;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public abstract class SectionModule implements Listener {

    private String moduleName;
    private QPlugin corePlugin;

    public SectionModule(String moduleName, QPlugin corePlugin) {
        this.moduleName = moduleName;
        this.corePlugin = corePlugin;
    }

    public abstract void enable();

    public void onEnable() {
        ServerUtils.registerListener(this, corePlugin);

        ServerUtils.log("Loading " + this.moduleName + "...");

        enable();
    }

    public String getModuleName() {
        return moduleName;
    }

    public void log(String string) {
        ServerUtils.log("[" + this.moduleName + "] " + string);
    }

    public QPlugin getCorePlugin() {
        return corePlugin;
    }

    public boolean isFileRetarded(File file) {
        return file.getName().equalsIgnoreCase(".DS_STORE");
    }
}
