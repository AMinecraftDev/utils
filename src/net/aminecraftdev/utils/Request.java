package net.aminecraftdev.utils;

import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Dec-17
 */
public interface Request<REQ> {

    List<REQ> get();

}
