package net.aminecraftdev.utils;

import org.bukkit.Color;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 14-Sep-17
 */
public enum ColorValues {

    WHITE(Color.WHITE),
    SILVER(Color.SILVER),
    GRAY(Color.GRAY),
    BLACK(Color.BLACK),
    RED(Color.RED),
    MAROON(Color.MAROON),
    YELLOW(Color.YELLOW),
    OLIVE(Color.OLIVE),
    LIME(Color.LIME),
    GREEN(Color.GREEN),
    AQUA(Color.AQUA),
    TEAL(Color.TEAL),
    BLUE(Color.BLUE),
    NAVY(Color.NAVY),
    FUCHSIA(Color.FUCHSIA),
    PURPLE(Color.PURPLE),
    ORANGE(Color.ORANGE);

    private String name;
    private Color color;

    ColorValues(Color color) {
        this.name = name();
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }

    public static Color getColor(String name) {
        for(ColorValues colorValues : values()) {
            if(colorValues.getName().equalsIgnoreCase(name)) return colorValues.getColor();
        }

        return null;
    }
}
