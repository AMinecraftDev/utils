package net.aminecraftdev.utils.chain;

import net.aminecraftdev.utils.ServerUtils;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 08-Dec-17
 */
public class ChainManager implements IChainManager {

    private IChain[] chains;

    public ChainManager(IChain[] chains) {
        for(int i = 0; i < chains.length; i++) {
            IChain chain1 = chains[i];
            IChain chain2 = chains[i + 1];

            ServerUtils.logDebug("Setting " + chain2.toString() + " as the successor to " + chain1.toString());
            chain1.setSuccessor(chain2);
        }

        this.chains = chains;
    }

    @Override
    public IChain[] getChains() {
        return chains;
    }

    @Override
    public void Execute(Object task) {
        IChain assignedChain = this.chains[0].canDoIt(task);

        if(assignedChain != null) {
            assignedChain.doTask(task);
            ServerUtils.logDebug("CoR task was completed by " + assignedChain.toString() + ".");
        } else {
            ServerUtils.logDebug("CoR task was not handled.");
        }
    }
}
