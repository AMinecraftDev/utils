package net.aminecraftdev.utils.chain;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 08-Dec-17
 */
public abstract class IChain {

    private IChain successor;

    public IChain getSuccessor() {
        return this.successor;
    }

    public void setSuccessor(IChain successor) {
        this.successor = successor;
    }

    public abstract void doTask(Object object);

    public abstract IChain canDoIt(Object object);

    @Override
    public abstract String toString();

}
