package net.aminecraftdev.utils.chain;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 08-Dec-17
 */
public interface IChainManager {

    IChain[] getChains();

    void Execute(Object task);

}
